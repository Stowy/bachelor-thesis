
#[
  = Interviews Notes

  #set text(size: 9pt)

  #table(
    columns: (auto, auto, auto, auto, auto, auto),
    // inset: 10pt,
    align: horizon,
    table.header(
      [],
      [*Felix "Xima" Maier*],
      [*Gabe Rundlett*],
      [*Ethan Gore*],
      [*Daniel "frozein" Elwell*],
      [*Summary*],
    ),
    [What lead you to start working with voxels?],
    [
      - side hustle
      - never liked triangle
      - sound more fun
      - like *minecraft* / pixel art
      - environment is not really dynamic
      - would love to see noita in 3D
    ],
    [
      - always interested
      - Minecraft
        - Command blocks
      - Interactable world
      - Meshes don't make sense
      - Blender sculpting uses voxels
    ],
    [
      - Inspired by the original alpha of Cube World in mid-2021.
      - Enjoyed the feel of the voxel style.Started working with voxels in September
        2021.
      - Continued due to the simplification of problems in voxel scenes, beneficial for
        a solo developer.
    ],
    [
      - other engine
      - opengl tutorials etc
      - videos
    ],
    [
      - Inspired by games like Minecraft and Cube World
      - Enjoy the dynamic nature of voxel worlds
    ],
    [For you, what is the hardest part when working with voxels?],
    [
      - data structures
      - focus if there's a large or small world
        - if theres a lot of dynamic stuff or not
        - minecraft has a good scale with that
      - finding the right balance between rendering and updating data structure
      - people often expect GI
      - they still have a lot of potential
      - more used on the scientific side
      - compression slows down updating
      - plan: go full on simulation scale with a small world
      - Non sparse octree
      - octree are slow when looking for neighbors
      - water simulation will be cool
      - noita
      - 3d texture with mipmaps
      - texture are fast because they use texture swizzling
      - can be 30% faster than buffers
    ],
    [
      - Too Much Data
      - Clever data structures
        - Should be compact
      - Compressed on the fly within a brick
      - Tree on top of that
      - Fast random access
      - Very low memory footprint
      - Still researching
      - Questions about ray tracing:
        - kajiya
          - Has world space irradiance cache
          - ReSTIR
          - Only one ray by 2x2 pixels for indirect
          - Shadow is only the sun with a shadow ray
          - Does spatiotemporal denoise
        - Can be blotchy in dark areas
          - Low chance of a ray to hit the light
          - Solving this is hard
          - Would like the lighting for every voxel be uniform for every voxels
          - Light in voxel
          - Occlusion culling using raster
        - Cellular automata has no noise
          - Uses a lot of memory
    ],
    [
      - Numerous voxel rendering techniques cause analysis paralysis.
      - Temptation to experiment with different techniques.
      - Data storage issues due to the volumetric nature of voxels.
    ],
    [
      - reduce memory consumption
      - naive
        - store everything
        - 2 level brickmaps
        - was still too much
      - now more compact
        - all data structures will be supported
        - all chucks can have a different stucture
    ],
    [
      - Memory consumption
      - Tradeoff between memory and updating speed of data structure
      - Should have fast random access
      - Expectations of users (GI)
      - Number of rendering techniques can cause analysis paralysis
    ],
    [From your personal experience, what do you think is the future of voxel
      rendering techniques ?],
    [
      - hope they can make the world more dynamic and have bigger world
      - more realistic minecraft
      - Brickmaps
        - Better balance
    ],
    [
      - Not enough experience
      - Towards ray tracing
      - Xima CO can use light for gameplay elements, not with ray tracing
      - Would be interesting to see if raytracing could have cellular automata qualities
        for gameplay
    ],
    [
      - Efficient primary ray rendering through standard rasterisation with proper
        culling.
      - Potential combination of rasterisation for primary rays and ray tracing for
        secondary rays.
      - Dual acceleration structures increase memory and generation time but may reduce
        ray usage.
    ],
    [
      - Still a lot of room
      - john lin
      - not sure
      - still exploring
    ],
    [
      - Hope to make worlds more dynamic
      - Inspired by John Lin
      - Brickmaps have a good balance of memory / update speed
      - Light computation technique could impact gameplay (e.g. cellular automata)
      - Use rasterisation for primary rays and ray tracing for secondaries
    ],
    [Based on what you've already done, what would be your suggestions to improve
      current voxel rendering?],
    [
      - We are at a limit in terms of compression
      - John lin was using a compression technique that was invented 10 years ago
      - Regarding denoisers and TAA they could profit from the fact that cubes are
        simple shapes
      - Xima has a denoiser like that, but more could be done
      - Voxels are easy if you need to do filtering
      - Make lighting constant accross faces
      - Gives a pixelated lighting
      - 20 lines of code
      - Upscaler
        - Uses a checkerboard upscaler
        - use the world space and normals
      - With voxels it's easier
    ],
    [
      - Storage could be improved
      - a lot of research for medical and movies (clouds)
      - Most popular by far is OpenVDB
        - Similar to an Octree but with 8x8x8 or 16x16x16 with bitmasks
        - Would be the best place to start
        - DAGs for static volume data
        - Nanite vertex data is turned to a DAG
        - Delta compression, RLE
      - If you want a tree with swaying leaves
        - Could bake it
        - Could have a bone system that could be modified and could be voxelized with SDFs
        - Could be too slow for an entire forest
    ],
    [
      - Do not overlook rasterisation; it's effective.
      - Ray tracing offers more interesting problem-solving opportunities.
    ],
    [
      - GI
        - old engine wasn't great
        - not started on new
      - speedup thanks to voxels
    ],
    [
      - Don't agree on whether storage could be improved or not (possible they don't talk
        about the same metric)
      - Denoisers could be designed around the simple shape of cubes
      - Upscaling could also profit from cubes
      - Rasterisation should still be considered
    ],
    [Have you used HWRT? If yes, what is your personal experience with it related to
      voxels?],
    [
      - 5 years ago, added HWRT to WGPU
      - Too slow for individual voxels
      - Chunk based might be good
      - HWRT is overkill
      - the TLAS is not optimal for AABB
      - it would be faster to write yourself
      - not limited to modern hardware
      - HWRT is a huge blackbox
      - John lin ranted about it
        - He gave up HWRT
      - Ray sorting is is not very usefull with chunk based HWRT
      - Mixing two hardware units could be a problem
      - BVH stuff is hard and boring
    ],
    [
      - Still learning
      - You can just pass a sparse representation
      - It only makes sense to give data from the surface
      - It's the same problem as meshing
      - Updating the TLAS is anoying because it adds cost
      - You can have anything in the "BLAS" with a custom intersection test
      - For trees you could evalutate the SDF the tree
      - Didn't try to have multiple Data structure in the BLAS
      - Only has a 8x8x8 brick using bitmasks
      - Prefers to do everything in compute
      - But HWRT is designed for the divergent nature of ray tracing
      - In compute, you can have threads finish early and have the rest work
      - Makes it so that the GPU has a lot of doing nothing
      - HWRT has a queue of work items and gives these works to utilise the GPU better
      - Makes it so you have better saturation of the GPU
      - New NVIDIA GPUs have shader excecution reordering
        - You can give a key value to the GPU and the GPU will resort that key to be in
          warps
        - They have the same cacheline
        - Could hit the same side of the branch (exit of the loop)
        - Can improve by up to 2x
      - NVIDIA cannot overlap any work on a single queue
        - Daxa doesn't have multi queue
      - Work graphs (only DX12 and AMD Vulkan) Could help for terrain generation
        - BVH traversal with workgraphs is slower than HWRT
      - Ray tracing work through trace could be use for example for terrain generation
        but haven't tried yet
    ],
    [
      No experience.
    ],
    [
      - HWRT will help
      - Is faster for now on big maps
        - small and big volumes
      - was sceptical
      - now thinks will be faster
      - Next step will be shadows
      - particles and waters are rasterised
    ],
    [
      - No consensus on if HWRT can be faster for voxels
      - The imposed "black box" TLAS is not designed for voxel data, one written by hand
        can be faster
      - Passing only surface data helps
      - Can be faster on big maps
      - HWRT is designed for the divergent nature of ray tracing (avoid threads being
        idle like in compute)
      - Ray reordering helps even more
      - Work graphs could help with that, but BVH traversal is slower with them
    ],
    [If you had to restart your project from scratch, is there anything that you
      would change?],
    [
      - Not really
      - It used to be a cpu-based voxel engine
      - Since he targets large scale world simulation
        - GPU Driven design
        - Would have been nice to start from that
        - The shift was compilcated, but now it's good
      - Has a fork where it's easy to prototype stuff
        - If things a good, it is merged to the main branch
      - GPU driven is hard to protype in (gabe rundlett gave up)
      - HWRT can start shaders from shaders
        - New extension might be better since it's made for that
    ],
    [
      - New to software architecture design
      - Still learning
      - Wouldn't start again
      - Keep learning and improve it in a continuous maner
      - For ex: terrain gen should be independant from rendering
      - Should be able to rip out part of the project
    ],
    [
      - Reconsider the scale of the engine.
      - Smaller scale might allow for testing new ideas not feasible at current scale.
    ],
    [
      - different GI
      - two compute shaders
        - primary, gets colours
        - then lighting shader for every visible shader
        - each voxel was 16 bytes
    ],
    [
      - GPU Driven rendering lends itself well to voxels
      - Having a second branch for rapid prototyping can help
      - Think of how big the project should be early on
      - Try to keep big modules separate (eg, terrain gen should be able to be changed
        without breaking rendering)
    ],
    [What is your personal experience with voxel animations ? ],
    [
      - Models made and animated with blockbench https://www.blockbench.net/
      - Models are voxelized in a 3d texture
      - Each entity would need its own 3d volume
      - 200k no problem to render
      - models in blockbench format with code based animations
      - jem -> voxel
      - \~11 Compute passes
      - Entities was harder than raytracing
      - Rendered using quads on the screen
      - entities don't have lighting yet
    ],
    [
      - Unexplored territory
      - Looked into animating pixel art
        - lot of things to learn from that
      - Still a lot to learn
    ],
    [
      No experience yet.
    ],
    [
      - was one of the hardest part
      - required to update the data continuously
      - the hardest part was allowing people to update stuff
      - stored a DF of each objects to check what objects people clicked on
      - keyframe
        - one volume per anims
        - have to save in flat array
      - maybe voxelize meshes
        - depends on size
      - static: dag
      - dynamic: brickmap or svo
    ],
    [
      - This is a hard problem
      - Can voxelize mesh model
        - Render entities to quads on screen
      - Can use a distance field
    ],
    [Is there anything we didn't mention on the subject of voxel rendering that you
      think would be worth mentioning ?],
    [
      - GPU driven rendering design is well suited for voxels
      - Large scale simulation friendly
      - Cellular automata is helped by the gpu driven rendering
    ],
    [
      - no
      - Most important is "Everything is dynamic"
      - If you're making a voxel project you should focus on the interactivity of the
        voxels
    ],
    [],
    [
      - impressed by the amount of work done in the space
      - have a lot of potential
      - not enough research
    ],
    [- Focus on dynamic worlds],
  )

] <no-wc>

