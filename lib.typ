#let my-star(center, ..style) = {
  (
    ctx => {
      // Define a default style
      let def-style = (n: 5, inner-radius: .5, radius: 1, stroke: auto, fill: auto)
      // Resolve center to a vector
      let (ctx, center) = cetz.coordinate.resolve(ctx, center)
      // Resolve the current style ("star")
      let style = cetz.styles.resolve(ctx.style, merge: style.named(), base: def-style, root: "star")
      // Compute the corner coordinates
      let corners = range(0, style.n * 2).map(
        i => {
          let a = 90deg + i * 360deg / (style.n * 2)
          let r = if calc.rem(i, 2) == 0 { style.radius } else { style.inner-radius }
          cetz.vector.add(center, (calc.cos(a) * r, calc.sin(a) * r, 0))
        },
      )
       
      let path = cetz.drawable.path(
        (cetz.path-util.line-segment(corners),),
        stroke: style.stroke,
        fill: style.fill,
        close: true,
      )
      (
        ctx: ctx,
        drawables: cetz.drawable.apply-transform(ctx.transform, path),
      )
    },
  )
}

#let appendices(body) = {
  pagebreak()
  counter(heading).update(0)
  counter("appendices").update(1)
   
  set heading(numbering: (..nums) => {
    let vals = nums.pos()
    let value = "ABCDEFGHIJ".at(vals.at(0) - 1)
    if vals.len() == 1 {
      return "APPENDIX " + value + ": "
    } else {
      return value + "." + nums.pos().slice(1).map(str).join(".")
    }
  });
   
  [#body]
}