#import "@preview/cetz:0.3.1"
#import "@preview/wordometer:0.1.4": word-count, total-words
#import "@preview/hydra:0.5.1": hydra
#import "@preview/codly:1.0.0": *
#import "lib.typ": *

#let project(
  title: "",
  subtitle: "",
  author: "",
  additionalInfo: "",
  date: none,
  abstract: [],
  body,
) = {
  // Set the document's basic properties.
  let min_page_for_header_footer = 9
  set document(author: author, title: title)
  set page(header: context{
    if counter(page).get().at(0) >= min_page_for_header_footer {
      if calc.odd(here().page()) {
        align(right, emph(hydra(1)))
      } else {
        align(left, emph(hydra(2)))
      }
      line(length: 100%)
    }
  }, footer: context{
    if counter(page).get().at(0) >= min_page_for_header_footer {
      if calc.odd(here().page()) {
        align(right, counter(page).display("1/1", both: true))
      } else {
        align(left, counter(page).display("1/1", both: true))
      }
    }
  }, numbering: "1")
  set text(font: "Libertinus Serif", lang: "en", region: "uk", size: 11pt)
  set heading(numbering: "1.1")
  show heading: set block(above: 1.4em, below: 1em)
  show bibliography: set heading(numbering: "1.1")
  set bibliography(title: "References", style: "harvard-cite-them-right")
  set raw(tab-size: 4, syntaxes: "Jai.sublime-synt")
  set math.equation(numbering: "(1)")
  show heading.where(level: 1): it => {
    // if calc.even(here().page()) {
    //   pagebreak()
    // }
    pagebreak(weak: true)
    it
  }
  show: word-count.with(exclude: (<no-wc>, raw))

  show raw: set text(font: "Monaspace Neon")
  show: codly-init.with()
  // codly(enable-numbers: true, zebra-color: white)
  // codly(zebra-fill: none)

  // set figure(placement: auto)
  show figure.caption: set text(font: "Source Sans 3", size: 10pt)

  // Title page.
  set align(center)
  v(1fr)

  text(18pt, author)
  linebreak()
  text(14pt, strong(additionalInfo))
  v(2em)

  text(14pt, "BACHELOR OF SCIENCE")
  linebreak()
  text(18pt, "Games Programming")
  v(2em)

  rect(width: 100%, [
    #text(2em, weight: 700, title)
    #v(-1em)
    #text(1.5em, weight: 700, subtitle)
  ])

  v(4em)

  text(12pt, "BACHELOR THESIS - 6GSC0XF101.2")
  linebreak()
  text(12pt, "Supervisors: Nicolas Siorak, Elias Farhan")
  v(2fr)
  text(1.1em, "SAE Institute Geneva")
  v(1em)
  text(1.1em, date)
  linebreak()
  [#total-words Words]

  v(1fr)

  set align(start + top)

  pagebreak()
  pagebreak()

  // Plagiarism page

  v(1fr)

  text(
    12pt,
    [
      I, #author, certify having personally written this Bachelor thesis.
      
      I also certify that I have not resorted to plagiarism and have conscientiously
      and clearly mentioned all borrowings from others.
    ],
  )
  align(right, image("img/sign.png", width: 35%))
  v(4em)
  text(12pt, [Geneva, #date])

  pagebreak()
  pagebreak()

  set par(justify: true)

  // Abstract page.
  align(
    center,
  )[
    #heading(outlined: false, numbering: none, text(0.85em, smallcaps[Abstract]))  
  ]
  abstract

  v(1.618fr)

  pagebreak()
  pagebreak()

  show outline.where(target: heading.where(outlined: true)): (it) => {
   show outline.entry.where(
      level: 1
    ): ent => {
      v(12pt, weak: true)
      strong(ent)
    }
    it
  }

  // Table of contents.
  outline(depth: 3, indent: auto, title: "Table of Contents")
  pagebreak()

  // Main body.
  body
}
