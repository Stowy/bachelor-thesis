set windows-shell := ["powershell.exe", "-NoLogo", "-Command"]

compile:
	typst compile main.typ --font-path ./fonts

watch:
	typst watch main.typ --font-path ./fonts