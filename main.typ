#import "template.typ": *
#import "@preview/glossarium:0.5.1": make-glossary, print-glossary, register-glossary, gls, glspl, agls
#show: make-glossary
#import "@preview/physica:0.9.3": *
#import "glossary.typ": entry-list


#show: project.with(
  title: "Voxel rendering using Ray Tracing",
  subtitle: "Implementation in a Vulkan application using Jai",
  author: "Fabian Huber",
  additionalInfo: [\#12-135542 / #link("mailto:fabian.hbr@protonmail.ch")[fabian.hbr\@protonmail.ch]],
  date: [July 19#super[th], 2024],
  abstract: [
    Voxel rendering is a technique used in computer graphics to represent
    three-dimensional objects through volumetric pixels (voxels).
    This method is particularly useful in applications such as medical imagery,
    terrain modelling, and game development.
    With the advent of high-performance graphics APIs like Vulkan,
    there is potential for significant improvements in the efficiency and quality of voxel rendering.
    The Jai programming language offers concise syntax and powerful features suited for graphics programming.

    This thesis aims to develop an efficient voxel rendering pipeline utilizing
    ray tracing within a Vulkan-based application.
    The implementation is carried out leveraging Jai's capabilities for improved
    development efficiency and performance.

    First, key terms such as voxels, ray tracing, ray casting, and path tracing are defined. Two families of voxel rendering techniques - surface extraction and volume marching — are presented.
    This study involves the integration of ray tracing techniques
    into a Vulkan application, programmed in Jai.
    Key aspects include the implementation of primary and secondary ray tracing
    for accurate light simulation,
    and voxel traversal algorithms for efficient rendering.
    Metrics such as frame time, memory consumption, and voxel counts are analysed.

    The research confirms that combining voxel rendering with ray tracing
    in a Vulkan application using Jai can achieve high-performance
    and high-quality results.
    These findings highlight the potential for further advancements in real-time graphics applications.
    Future work will explore additional optimization techniques
    and the incorporation of more complex lighting models
    to enhance rendering fidelity.

    *Keywords*: Voxel Rendering, Ray Tracing, Vulkan, Jai Programming Language, Real-Time Graphics, Performance Optimization
  ],
)

#heading(level: 1, numbering: none, "Glossary")

#register-glossary(entry-list)
#print-glossary(entry-list)

#pagebreak(weak: true)

= Introduction

_Minecraft_ (2011), the most bought game with three hundred million copies sold
@minecraft-300-million, has the player explore and build in a voxel world. In
2020, _Teardown_ was released featuring ray tracing and a fully destructible voxel world,
selling almost three million units @teardown-sell-figures.

Voxels are often imagined and portrayed as cubes, but as it will be explained
later, it is more accurate to understand them as being a value on a regular
grid.

Lighting computation has long been a challenge for interactive graphics.
Direct lighting can be computed in real-time,
but indirect lighting is usually harder to calculate and therefore pre-computed.
This is a technique called Lightmaps introduced in _Quake_ (1996) @lightmaps.
Combining direct lighting and indirect lighting leads to having
global illumination. It is often accomplished using ray tracing. The discrete
nature of voxels allows the simplification of the problem of ray intersection tests.
Voxels are even used with traditional #glspl("mesh") for global illumination by
voxelizing the mesh data @cone-tracing.

There are other uses for voxels.
For example, in medical imagery, MRI and ultrasound scans are stored using them.
_The Legend of Zelda: Tears of the Kingdom_ (2023) has a voxel representation of the world.
They are not directly displayed, but used for gameplay elements and to simulate sound propagation @tunes-of-the-kingdom.

The primary goal of this bachelor will be to implement a voxel ray tracing in a
Vulkan application using the Jai programming language. This will be done in a
compute shader, which in part justifies the choice of Vulkan as a graphics @api.
Its predecessor, OpenGL, does not natively support compute shaders and would have
required to either use OpenCL, or do the work in a fragment shader.
DirectX could have been used, but it is not cross-platform and is not an open standard.

The plan of this thesis is the following: first a state of the art about voxel
rendering will be presented. It will contain various definitions, voxel storage
techniques and examples in games. Then, in the qualitative analysis part, a game and an engine will be analysed more in depth and the result of interviews done with
experts will be presented. These are _Teardown_ and _GVox Engine_.
The former because it features a fully destructible voxel world with ray tracing and is a successful commercial game.
The latter because it is an open-source voxel engine with path tracing and has world stored using brickmaps, which is a data structure presented in @chap-brickmap.
After that, the test protocol of the media project will be explained.
The key metrics are frame time, memory consumption and the amount of voxels.
This will be followed by a production overview of the
media project. The quantitative analysis will then be used to examine the
measurements done the media project.

= State of the Art

This section will present various techniques used to render voxels. Then, it will
show how they are applied to various projects.

== Definitions

The term voxel comes from the contraction of the words "volume" and "pixel".
Pixel itself comes from "picture element". We can then understand voxel as being
a "volume element" @real-time-rendering. Although they are often portrayed as
cubes (see @minecraft-1), it is more accurate to think of them as a value on a
regular grid in 3D space.

#figure(
  image("img/minecraft-1.jpg", width: 80%),
  caption: [Screenshot of the game _Minecraft_, which uses voxels. @minecraft],
) <minecraft-1>

Using voxels in games is not new: in 1998, after the development of _Quake II_ (1997),
John Carmack explained that he tried using them for _Quake III_ (1999).
He decided against it since doing so was too slow
without the use of specialized hardware @carmack-ray-casting.

Indeed, graphics cards are very good at displaying triangles
@real-time-rendering, and at the time you couldn't run arbitrary code for each
pixels like today. However, this restriction lead developers to directly use
rasterisation to render voxels. Rasterisation, in this context, is the process by
which a triangle (or any other primitive shape) is transformed to a series of
pixels. This method is explained in more depth in @surface-extraction.

But this is not what Carmack tried for _Quake III_.
He was using a technique called ray casting.
The idea is that instead of transforming 3D triangles to 2D
and then rasterising them, rays are shoot from the camera to check if they
collide with some geometry. The geometry can be of any kind; for example, _Wolfenstein 3D_ (1992),
also worked on by John Carmack, casts rays against a 2D grid and uses the
distance of those rays to simulate 3D @wolfenstein-ray-casting. See
@wolfenstein. In the context of voxel rendering, it would mean casting rays
against a voxel grid.

#figure(
  image("img/wolfenstein.jpg", width: 70%),
  caption: [Screenshot of the game _Wolfenstein 3D_, which uses ray casting to simulate 3D.
    @wolfenstein-3d],
) <wolfenstein>

Recently, there has been an increase in interest in another technique by the
name of ray tracing. Sometimes it is used interchangeably with ray casting, but
it is more helpful to consider ray casting as some way of doing ray tracing. Ray
tracing is generally used to refer to the act of using rays to simulate the
behaviour of light. There are multiple methods for that, one of
which is path tracing.

As previously stated, path tracing is a way of doing ray tracing. Its
specificity is that it will recursively bounce rays in a way that will give a
realistic feeling to the lighting of a scene. This algorithm allows for Global
Illumination (GI), which is when the lighting is not only considered directly (direct
illumination) but indirect bounces are also used (indirect illumination). This
can be observed in @path-tracing-image. The difference between ray casting, ray
tracing, and path tracing is illustrated in @ray-trace-cast-path.

#figure(
  image("img/path-tracing.jpg", width: 60%),
  caption: [An image rendered using path tracing, demonstrating notable features of the
    technique. @wikipedia-path-tracing],
) <path-tracing-image>

#figure(
  cetz.canvas({
    import cetz.draw: *
    circle((0, 0), radius: (4.5, 3))
    content((0, 2), [Ray Tracing])
    circle((-2, 0), name: "ray-casting", radius: 1.5)
    content("ray-casting", [Ray Casting])
    circle((2, 0), name: "path-tracing", radius: 1.5)
    content("path-tracing", [Path Tracing])
  }),
  caption: [Illustration of the classification of ray casting, ray tracing and path
    tracing.],
) <ray-trace-cast-path>

To be able to use #glspl("gpu"), developers can use graphic #glspl("api"). These
abstract the underlying machine, to allow to write software once for multiple
graphics cards. Some of these #glspl("api") come from the @os, such as DirectX on _Windows_ and
Metal on _MacOS_. On the other hand, open standards have been developed to have
cross-platform graphic #glspl("api"). OpenGL was created in 1992 by Silicon
Graphics @beginning-opengl but was then transferred to the Khronos Group in 2006
@opengl-wikipedia. Its successor Vulkan, which was originally
called OpenGL Next, was released in 2016. It is meant to be a lower-level @api
and allows for more control over the @gpu. Most importantly for voxel rendering,
it allows the use of compute #glspl("shader") directly, which was not the case
with OpenGL since it required to use OpenCL for that @vulkan-wikipedia.
Apart from surface extraction explained in @surface-extraction, voxel rendering
techniques usually require the use of compute shaders since they are not based
on the traditional triangle pipeline.

== Current Voxel Rendering Techniques

Multiple methods to render voxels have been developed throughout the years, both
on the @cpu and on the @gpu. This was pushed by both medical imagery and video
games.

=== Surface Extraction <surface-extraction>

This method is the most commonly used one in video games; it is notably used by _Minecraft_.
The idea is that the surface of the voxel data is first converted to a triangle
@mesh before being sent to the @gpu. Just turning every voxel into a cube would
be inefficient as most of the triangles would be hidden by neighbouring
voxels. Therefore methods are usually used to reduce the number of triangles.

The first one is to check around the current voxel if it has any neighbours.
If yes, the face between the current voxel and the neighbour will not be meshed. Additionally, other methods can be used,
such as greedy meshing, which will look for neighbouring voxels of the same type
and will extend the triangles to them @greedy-meshing-mc-game. This is
illustrated in @greedy-meshing.

#figure(
  grid(
    columns: 2,
    gutter: 28pt,
    cetz.canvas({
      import cetz.draw: *
      grid((0, 0), (4, 4), step: 2)
      line((0, 0), (4, 4))
      line((0, 2), (2, 4))
      line((2, 0), (4, 2))
      content((2, 4.5), "Without Greedy Meshing")
    }),
    cetz.canvas({
      import cetz.draw: *
      rect((0, 0), (4, 4))
      line((0, 0), (4, 4))
      content((2, 4.5), "With Greedy Meshing")
    }),
  ),
  caption: [2D example of greedy meshing. In 3D, it is done on each axis separately, so the
    principle is the same.],
) <greedy-meshing>

There are various other ways to accelerate the rendering of voxels using
rasterisation, such as culling, vertex compression and instancing but these will
not be explained here as it is outside the scope of this study.

Using triangles may have the advantage of being able to use Hardware Ray
Tracing (HWRT) since it is designed with the traditional graphical pipeline in
mind. But using custom intersection code can allow to do HWRT without
triangles.

=== Volume Marching

Volume marching is the family of techniques that use rays that traverse some
kind of data structure. So they all use ray tracing and each data structure has
its pros and cons.

==== Fast Voxel Traversal Algorithm

In their paper, #cite(<fast-voxel-traversal-algorithm>, form: "prose") present
an algorithm for traversing voxels using rays without checking the collision
against every one of them. This allows to only check voxels that are traversed
by the ray. However, this method stores the voxels in a 3D grid, meaning that if there
is a significant amount of empty space, the ray still has to check collisions against each empty voxel.
This is illustrated in @fast-voxel-traversal-figure.

#figure(
  cetz.canvas({
    import cetz.draw: *
    grid((0, 0), (10, 4))
    set-style(mark: (end: (symbol: ">")))
    line((5, -0.5), (10.5, 3.5))
    content((5.5, 0.5), "a")
    content((6.5, 0.5), "b")
    content((7.5, 0.5), "c")
    content((7.5, 1.5), "d")
    content((8.5, 1.5), "e")
    content((8.5, 2.5), "f")
    content((9.5, 2.5), "g")
    content((9.5, 3.5), "h")
  }),
  caption: [Illustration of the Fast Voxel Traversal Algorithm. Instead of checking the
    whole grid for a collision, it allows us to only check voxels a, b, c, d, e, f,
    g and h.],
) <fast-voxel-traversal-figure>

This algorithm is at the heart of every other voxel ray tracing method, so
although it is rarely used by itself, it was improved later on. This way of
traversing voxels is also commonly called a Digital Differential Analyser (DDA).

==== Sparse Voxel Octrees

Sparse Voxel Octrees (SVOs) are a way to profit from the sparsity of most voxel
data sets.
The idea is to group voxels in a big voxel that is itself part of a bigger voxel.
The advantage comes from the fact that if a
parent node does not have children, it can just be marked as empty and the
traversal does not have to check inside it. This is illustrated in
@figure-sparse-voxel-octree.

#figure(
  image("img/Octree2.svg", width: 70%),
  caption: [Schematic drawing of an octree. @wikipedia-octree],
) <figure-sparse-voxel-octree>

This technique was explored by #cite(<efficient-sparse-voxel-octrees>, form: "prose").
In their paper, they present a ray cast algorithm that traverses the SVO using a
@stack. As they explain, this is quite costly for a @gpu, as their architecture
is not well suited for that kind of data structure. Another drawback of SVOs is
that they are quite costly to update, which is not ideal for video games since
interacting with the world is a big part of what makes voxel games attractive.

==== Sparse Voxel Directed Acyclic Graphs

Sparse Voxel Directed Acyclic Graphs (SVDAGs) can be seen as a modification of
SVOs. They try to reduce the memory consumption of SVOs by finding similar nodes
in the tree and allowing a child node to be referenced by multiple parents. This has
the effect of reducing repetitions in that data structure. You can see a
comparison of SVOs and SVDAGs in @figure-dag.

#figure(
  image("img/dag.jpg", width: 70%),
  caption: [Comparison of SVOs and SVDAGs. @symmetry-aware-dags],
) <figure-dag>

This comes with the drawback that if you modify a voxel that is in a node
referenced by two parents, it can be quite computationally intensive to update
the SVDAG. This is a problem that #cite(<modifying-dags>, form: "prose") have
tried to address in their paper. They made an interactive prototype application
where you can edit scenes at a resolution of 128'000#super("3").

==== Brickmaps <chap-brickmap>

As previously mentioned, SVOs and SVDAGs have the disadvantage of slowing down
the edition of the voxel grid with the trade off of accelerating the rendering.
This is something that #cite(<brickmaps-paper>, form: "prose") has tried to
solve using a novel approach called Brickmaps. This technique also provides a
solution to @streaming the voxel data to the @gpu when it is needed as well as #agls("lod") system.

This data structure works by having multiple levels of grids. The cells of each
level point to multiple cells of the lower level until the bottom level where
the voxels are stored using #glspl("bitmask") and some colour information. Since
a higher-level cell can either point to more data or be empty, brickmaps are
similar to SVOs as they allow to skip empty space efficiently.

The bottom level grid is called the brickmap. It has a size of 8#super("3") and
is the one that stores the voxel data. One level higher is the brickgrid, where
the cells either point to a brickmap or are empty. It can be of any size.
Finally, there is the higher-level grid, which points to a group of brickgrid
cells. It is possible to have multiple levels of higher-level grids. This
structure is illustrated in @fig-brickmaps.
The brickmap is in blue and the brickgrid in red.
The filled squares in the grid are non-empty cells.
The green grid is the higher level grid.
Each grid level is traversed using
the DDA algorithm by #cite(<fast-voxel-traversal-algorithm>, form: "prose").

#figure(
  cetz.canvas({
    import cetz.draw: *
    set-style(mark: (end: (symbol: ">")))

    grid((0, 0), (2, 2), step: 0.25, stroke: blue)
    content((1, -0.1), "Brickmap", anchor: "north")

    grid((3, 0), (5.5, 2.5), step: 0.25, stroke: red)
    rect((3, 0), (3.25, 0.25), stroke: none, fill: maroon)
    rect((4, 2), (4.25, 2.25), stroke: none, fill: maroon)
    rect((4.25, 2), (4.5, 2.25), stroke: none, fill: maroon)
    rect((4.5, 2), (4.75, 2.25), stroke: none, fill: maroon)
    rect((3.25, 0.75), (3.5, 1), stroke: none, fill: maroon)
    content((4.25, -0.1), "Brickgrid", anchor: "north")

    line((3.375, 0.875), (2, 1))

    grid((3, 2.5), (4, 1.5), step: 0.5, stroke: green)
    content((2.2, 2.8), "Higher level grid", anchor: "south-east")
    line((2.2, 2.8), (3, 2.5))
  }),
  caption: [Illustration of how brickmaps work. ],
) <fig-brickmaps>

As outlined earlier, brickmaps have a way to efficiently handle #glspl("lod") and
data @streaming. To do that, each brickgrid and higher-level cells store #agls("lod")
colour. This value is used for rendering when the lower-level is either not
loaded or too small to profit from the finer details. When a voxel is rendered
using #agls("lod") colour because the lower-level is not loaded, the cell
@id-gloss is put in a shared @buffer between the @cpu and the @gpu. This is then
used after the rendering of the scene to know which data should be loaded.

==== Hardware Ray Tracing (HWRT)

In 2018, _NVIDIA_ unveiled their Turing architecture @nvidia-turing, being the
first series of graphics cards for the general public with hardware-accelerated
ray tracing. They came with a new set of tools in graphics #glspl("api") that
allowed programmers to use these cores, namely DXR (DirectX Raytracing) in
DirectX 12 @announcing-dxr and the Vulkan Ray Tracing Extensions in Vulkan
@blog-raytracing-in-vulkan. They allow access to five new #glspl("shader")
@real-time-rendering:

+ Ray generation @shader
+ Closest hit @shader
+ Miss @shader
+ Any hit @shader
+ Intersection @shader

The hardware ray tracing pipeline has features to accelerate the collision check
of rays against the data. This is done using acceleration structures separated
into two parts, the Top-Level Acceleration Structure (TLAS) and the Bottom-Level
Acceleration Structures (BLAS). There is only one TLAS, but there are multiple
BLAS. The user has no control over the TLAS, as it is implementation-defined.
Yet, it is often implemented using a @bvh. There are
two kinds of BLAS: triangle groups and analytical/procedural. Procedural BLAS
are defined using an intersection @shader. This is illustrated in @blas-tlas.

#figure(
  image("img/blas-tlas.jpg", width: 70%),
  caption: [Illustration of how the Top-Level Acceleration Structure (TLAS) is connected to
    a set of Bottom-Level Acceleration Structures (BLAS). @real-time-rendering],
) <blas-tlas>

There have been few projects using hardware ray tracing to render
voxels. Recently, #cite(<frozein-ray-tracing>, form: "prose"), a notable voxel
project author and YouTuber, exposed his method in a video. In it, he explains
that he created a custom intersection @shader that is tasked with checking the
collision of a ray against a voxel volume using the DDA algorithm. The BLAS
contains multiple voxels. This means that it is possible to move these volumes
continuously if that is desired.

A potential drawback of using HWRT for voxel rendering is that you're profiting
less from the speed that ray tracing voxels has compared to data like triangles.
That is because the ray has to first traverse the TLAS which is likely to be a
@bvh before being able to check a collision against a voxel. On the other hand,
the @bvh could have the advantage of helping sparse voxel data, as empty space
could just be skipped. Whether the ray collision check is accelerated by the @bvh
or not, there is always the cost of building it to take into consideration.

== Global Illumination with Path Tracing

Computing global illumination using recursive techniques was pioneered by #cite(<whitted-ray-tracing>, form: "author") in #cite(<whitted-ray-tracing>, form: "year") in
his paper _An improved illumination model for shaded display_. His technique
allows to have true shadows, reflections and refraction as illustrated in
@fig-whitted.

#figure(
  image("img/spheres-500x500.jpg", width: 50%),
  caption: [Image rendered using Whitted style ray tracing @whitted-ray-tracing],
) <fig-whitted>

His model may be written as

$
  I = I_a + k_d sum_(j=1)^(j="ls")
  (arrow(n) dot arrow(l_j)) + k_s S + k_t T
$

where

- $I$ is the light intensity
- $I_a$ is the ambient light
- $k_d$ is the diffuse reflection constant
- $sum_(j=1)^(j="ls")$ is the sum of every light source in the scene
- $arrow(n)$ is the normal of the surface
- $arrow(l_j)$ is the light direction
- $k_s$ is the specular reflection coefficient
- $S$ is the intensity of light incident from the $arrow(r)$ direction (reflection
  direction)
- $k_t$ is the transmission coefficient
- $T$ is the intensity of light from the $arrow(p)$ direction (refraction
  direction)

$k_s$ and $k_t$ where constants for the image in @fig-whitted but they should be
a function that incorporates the Fresnel reflection law.
As for the direction $arrow(p)$, it should obey Snell's law.

Although this model allows to have some optics effects, it is not physically
based. #cite(<microfacet-model>, form: "prose") have proposed a model based on
microfacets theory in their paper _A Reflectance Model for Computer Graphics_.
According to de Vries, for a lighting model to be considered physically based it
needs to:
#quote(
  attribution: "de Vries",
  [(1) Be based on the microfacet surface model (2) Be energy conserving (3) Use a
    physically based @brdf],
)

The goal of modern @pbr is to compute the rendering equation presented by #cite(<rendering-equation>, form: "prose").
This equation may be written as

$
  L_o (arrow(p), arrow(omega)_o) =
  L_e (arrow(p), arrow(omega)_o) +
  L_r (arrow(p), arrow(omega)_o) \
  L_r (arrow(p), arrow(omega)_o) =
  integral_Omega f_r (arrow(p), arrow(omega)_i, arrow(omega)_o)
  L_i (arrow(p), arrow(omega)_i)
  (arrow(omega)_i dot arrow(n)) dd(arrow(omega)_i)
$

where

- $L_o (arrow(p), arrow(omega)_o)$ is the total radiance (which will be explained
  later) at a point $arrow(p)$ at an outgoing direction $arrow(omega)_o$
- $arrow(p)$ is the point is space
- $arrow(omega)_o$ is the direction of outgoing light
- $L_e (arrow(p), arrow(omega)_o)$ is the emitted radiance
- $L_r (arrow(p), arrow(omega)_o)$ is the reflected radiance
- $integral_Omega dots dd(arrow(omega)_i)$ is an integral over the hemisphere $Omega$
- $Omega$ is the hemisphere around the normal $arrow(n)$ that contains every
  values for $arrow(omega)_i$ where $arrow(omega)_i dot arrow(n) > 0$
- $f_r (arrow(p), arrow(omega)_i, arrow(omega)_o)$ is the @brdf or the amount of light that is
  reflected from $arrow(omega)_i$ to $arrow(omega)_o$ at point $arrow(p)$
- $arrow(omega)_i$ is the opposite direction of the incoming light
- $L_i (arrow(p), arrow(omega)_i)$ is the radiance arriving at $arrow(p)$ from the
  direction $arrow(omega)_i$
- $arrow(n)$ is the normal of the surface at point $arrow(p)$
- $(arrow(omega)_i dot arrow(n))$ is the attenuation factor of the irradiance due
  to the incident angle, which takes into account the spreading of the radiance on
  the surface when the lighting is not perpendicular. It can be simplified to $cos theta_i$ and
  is often called the geometry term.

Radiance can be defined as the radiant flux emitted, reflected, transmitted or
received by a given surface, per unit solid angle per unit area. The solid
angle, noted as $omega$, is the area of a shape projected onto a unit sphere.
The area is the one in which point $arrow(p)$ lies. Radiant flux, noted as $Phi$,
is the total energy of a light source over every wavelength.

#figure(
  image("img/radiance.png", width: 50%),
  caption: [Illustration of radiance over an area $A$ and the solid angle $omega$.
    @learn-opengl-pbr],
) <radiance-learn-ogl>

Since we want all incoming light, we need to have the value of radiance over
every solid angle value. This is called irradiance and is therefore defined as
the radiant flux received by a surface per unit area. However, if we think of
how the integral can be computed, we can, for example, sum up the incoming light
over the hemisphere. In this context, we would only consider one direction on
the hemisphere at a time, which would be the radiance and then, once this energy
is projected onto the surface, it becomes irradiance. In the rendering equation,
this is done by multiplying by the geometry term. To conclude, $L_i (arrow(p), arrow(omega)_i)$ is
the incoming radiance, $L_i (arrow(p), arrow(omega)_i) (arrow(omega)_i dot arrow(n))$ is
the irradiance and then the @brdf is applied to the irradiance.

One way of solving this equation using ray tracing would be to shoot rays from
our surface towards light sources, a point light for example, and check if the surface
should be lit, determined by if a ray hits something before hitting the light.
Point lights represent an infinitely small light source, which
does not exist in real life, and gives hard shadows. One way to remedy that,
would be to have a light that has an area. Points would then be randomly
generated on this area and rays can be traced towards them. This would give
smooth shadows, but requires to shoot more than one ray, and the more rays are
shot, the less noisy the shadows will be. This process is called ray-traced
shadows.

This process only considers direct lighting and is therefore not path tracing.
To have indirect lighting, we must compute the integral in a recursive manner,
until reaching the sky or the sun or if the maximum number of bounces is reached.
With this technique, rays are shot from the camera and upon hitting a surface, a
random direction is chosen on the hemisphere directed along the surface normal @wikipedia-path-tracing.

If you have an equal chance of picking any value over the hemisphere, the
sampled value must be divided by a @pdf which, in this case, is a constant equal
to $1 / (2 pi)$ since $2 pi$ is the area of the hemisphere @ray-tracing-voxel.

One way to reduce noise is to use importance sampling using a cosine weighted
diffuse direction. The @pdf must then be changed to $(arrow(n) dot arrow(r)) / pi$ where $arrow(r)$ is
the cosine weighted random diffuse direction. Pseudocode for a simple path
tracer without reflection and refraction can be seen in @lst-path-tracer.

#figure(
  ```glsl
  vec3 Trace(Ray ray) {
    const vec3 sky_colour = vec3(1.2, 1.2, 1.0);
    vec3 throughput = vec3(1.0);
    for (int bounce = 0; bounce < 4; bounce++) {
      FindResult find_result = FindNearestObject(ray);
      if (find_result.is_empty) return throughput * sky_colour;

      vec3 brdf = find_result.base_colour / PI;
      vec3 new_direction = CosWeightedDiffuseReflection(find_result.normal);
      float pdf = dot(find_result.normal, new_direction) / PI;
      throughput *= brdf * (dot(find_result.normal, new_direction) / pdf);
      ray = Ray(find_result.collision_point, new_direction);
    }

    return vec3(0.0);
  }
  ```,
  caption: [Pseudocode for a simple path tracer.],
) <lst-path-tracer>

== Voxel in Video Games and Existing Engines <voxel-in-video-games>

This section contains a brief explanation of how some projects have chosen to
render voxels. _Teardown_ and _GVOX Engine_ are analysed more thoroughly
respectively in @teardown-title and @gvox-title.

_RenderDoc_, an open-source graphics debugger for capturing and
analysing frames in graphics applications,
was used to see how some of these games work.

_Minecraft_ sparked significant interest in voxels. As can be seen in
@mc-renderdoc, it uses surface extraction as a rendering technique.
To handle its infinite maps, the world is split into chunks, that are each rendered
separately.
In the figure, you can see the draw call of a chunk with its @mesh data.

#figure(
  image("img/minecraft-renderdoc.jpg", width: 60%),
  caption: [Screenshot of a _RenderDoc_ capture of _Minecraft_.],
) <mc-renderdoc>

To handle textured voxels, _Minecraft_ uses a texture atlas, as this allows to
render an entire chunk at once without having to render each voxel type
individually. This has the consequence of not being able to use greedy meshing
as you can't use texture repetition.

To handle lighting, each empty voxel has a lighting value between 0 and 16 that
decreases when not exposed to the sky or during the night.

Since _Minecraft_'s lighting model is pretty simple, users have created mods
that allow the creation of custom #glspl("shader"). With them, programmers have
been able to create more complex lighting as can be seen in @mc-shader. These
#glspl("shader") are implemented using the traditional graphical pipeline. This
is presented in a video by #cite(<acerola-minecraft>, form: "prose").

#figure(
  image("img/minecraft-2.jpg", width: 70%),
  caption: [Screenshot of _Minecraft_ with the _Sildur's Vibrant Shaders_.],
) <mc-shader>

_Cube World_ @cube-world is an RPG well known for its voxel look. The techniques
the game utilizes are similar to the ones used in _Minecraft_. It uses surface
extraction and sends the data to the @gpu in chunks (see @cubeworld-renderdoc).
It also does not use greedy meshing. Where it differs from _Minecraft_ is that
instead of using textures, it uses vertex colours, so the voxels all have a
uniform colour.

#figure(
  image("img/cubeworld_renderdoc.jpg", width: 60%),
  caption: [Screenshot of a _RenderDoc_ capture of _Cube World_.],
) <cubeworld-renderdoc>

Outside of finished games, some people have started to write voxel engines and
share their progress on the Internet. One example is _Ethan Gore's Voxel Project_,
which has a very high level of detail. Ethan Gore hasn't disclosed many details on how
this is achieved, and the engine is closed source.
However, he mentioned in an FAQ that he does use rasterisation and not ray tracing @ethan-gore-faq, emphasizing that significant gains derive from a robust @lod system.
Using _RenderDoc_, it is possible to gain more information.
It seems like the drawing of voxels is separated into chunks which are also separated by faces.
This means that the engine starts by drawing the faces that are in the direction of the up axis and
then continues with the other ones. This is visible in @ethan-renderdoc.
You can see that some chunks are not yet drawn and that the engine does not draw every axis at the same time.

#figure(
  image("img/ethan-renderdoc.jpg", width: 70%),
  caption: [Image taken in the middle of the rendering of a frame in _Ethan Gore's Voxel Project_.],
) <ethan-renderdoc>

Another voxel engine that is often showcased in _YouTube_ videos is _Octo Voxel_.
In one of these, #cite(<douglas-devlog-4>, form: "prose") explains the rendering
techniques he uses. His goal was to be able to run the engine on #glspl("igpu"),
so although he started with a ray tracing-based renderer, he switched to a
technique he called "parallax ray marching". It works by rendering boxes through
the rasterisation pipeline and then doing ray tracing in the fragment @shader.
This is efficient because you only need to check for collisions inside that box,
and thus allows to skip empty space. However, it has the limitation of not
allowing to bounce rays around the scene. So lighting effects need to use
rasterisation techniques such as shadow mapping.

_Octo Voxel_ was recently switched back to ray tracing to avoid these
limitations. Although the switch is not complete, the new renderer was showcased
in a video where #cite(<douglas-devlog-17>, form: "prose") shows it rendering
scenes using ray tracing to test for shadows. This can be seen in
@fig-octo-voxel under the trees.

#figure(
  image("img/octo-ray-tracing.jpg", width: 70%),
  caption: [Screenshot of the latest version of _Octo Voxel_ using ray tracing as a
    rendering technique. @douglas-devlog-17],
) <fig-octo-voxel>

== Media Project Objectives

The media project of this bachelor will be about implementing a voxel renderer
using Vulkan and Jai. The goal is to have a desktop application that can run on
both Windows and Linux using the cross-platform abilities of Vulkan. It would be
able to load voxel scenes that are in the `.gvox` format @gvox. If this turns
out to be too hard, it is possible that the project uses the `.vox` format
@voxel-model. And if that also fails, it is possible that a hard coded version
will be used. Ideally, it would demonstrate the ability to use advanced
rendering techniques that are possible using ray tracing such as global
illumination (or some form of indirect illumination).

The Jai programming language is a project started by Jonathan Blow in 2014 as a
C++ replacement to support his video game project, Sokoban. The Jai compiler has
significantly expanded since its inception, incorporating features like an @llvm
backend, macros, and inline assembly support. As of may 2024, the closed beta
includes over 600 members. Jai aims for high performance, targeting the
compilation of 1 million lines of code in under one second, currently achieving
250,000 lines per second with the x64 backend. Jai is an imperative, statically
typed language offering metaprogramming, multiple return values, and operator
overloading, while excluding elements like garbage collection, exceptions, and
object-oriented inheritance. Supporting Windows, Ubuntu Linux, MacOS and others, Jai's syntax
is still provisional, with refinements anticipated before its official release
alongside Blow's game, Sokoban.

The technique that will be used to store and render voxels will start by a
simple array. Ideally, it would be upgraded to brickmaps, explained in
@chap-brickmap. If this succeeds, it will be extended to allow the usage of
different kinds of materials, which is necessary for global illumination,
similar to the system introduced by #cite(<novel-extended-brickmaps>, form: "prose").

It is uncertain if it will be possible to have global illumination in the time
available for this project. Furthermore, as the Brickmap paper was programmed in
@cuda, the Vulkan implementation may prove to be more difficult, especially on
the data @streaming part.

The media project is planned to use these libraries:

- *SDL2*: Cross-platform window management
- *Vulkan Memory Allocator*: Easy to integrate Vulkan memory allocation library
- *ImGui*: Allows to easily create GUI tools
- *Gvox*: Allows to load `.gvox` files that contain voxel models

Some of these have wrappers provided with the Jai compiler, but others may
require to write bindings for. However, Jai comes with a tool to automatically
generate bindings from C/C++ headers.

To test the media project, multiple variables will be measured. Performance
being the most important one, frame time (ms) will be timed.
A challenge with voxel data is memory consumption; this will also be
measured. The compression ratio of a data structure impacts its building
performance, so along with traversal speed, building speed will be measured brickmaps are implemented.
It will be compared to other voxel engines if possible and pertinent. It is also
possible that the media project will be compared to itself before and after a
specific optimization has been applied.

== Summary

In this section, we looked at definitions that are important to know in the
context of voxel ray tracing, such as ray casting and path tracing. We also
looked at the two main families of voxel rendering in the name of surface
extraction and volume marching. We then looked at papers that explain how to
implement ray tracing, such as the fundamental Fast Voxel Traversal Algorithm.
An overview of the use of HWRT was also studied. Then, the rendering techniques
of various games and engines were explained and analysed, followed by a presentation of the
planned media project which was helped by the former analysis.

= Qualitative Analysis <qualitative-analysis>

This section will contain an analysis of two notable voxel projects, _GVOX Engine_ and _Teardown_,
and then a presentation of interviews done with experts in voxel rendering. This
will help us to fulfil the objectives of the media project.

== Voxel Renderers Analysis

=== Teardown <teardown-title>

_Teardown_ is a sandbox puzzle game released by #cite(<teardown-game>, form: "prose").
It uses a custom voxel engine made by Dennis Gustafsson. This is an interesting
project to analyse because it contains levels made of destructible voxels. It
also features fully dynamic lighting that uses ray tracing. Furthermore, the
objects don't have to align to the voxel grid, because it is also based on a
technique similar to parallax ray marching in _Octo Voxel_ as described in
@voxel-in-video-games.

#figure(
  image("./img/teardown-gbuffer/00-final.jpg", width: 70%),
  caption: [Screenshot of the game _Teardown_. @teardown-game @teardown-frame-teardown],
) <fig-teardown>

This analysis is based on three sources: a _Twitch_ stream presented by Dennis
Gustafsson @teardown-technical-dive and two blog posts on the rendering of _Teardown_.
Namely _Teardown Frame Teardown_ @teardown-frame-teardown and _Teardown Teardown_ @teardown-teardown.

_Teardown_ uses @deferred-rendering, meaning that it first starts by rendering
the geometry @buffer (G-buffer). Its layout is described in @gbuffer-teardown
and a breakdown on how to make the image in @fig-teardown is available in
@gbuffer-teardown-image.

#figure(
  table(
    columns: (auto, auto, auto, auto, auto, auto),
    align: horizon,
    table.header(
      table.cell(stroke: none)[],
      [*R*],
      [*G*],
      [*B*],
      [*A*],
      table.cell(stroke: none)[],
    ),
    table.cell(fill: blue.lighten(60%))[*Base colour*],
    table.cell(fill: blue.lighten(60%))[Red],
    table.cell(fill: blue.lighten(60%))[Green],
    table.cell(fill: blue.lighten(60%))[Blue],
    table.cell(fill: gray)[],
    table.cell(stroke: none)[`RGB16_UNORM`],
    table.cell(fill: green.lighten(60%))[*Normal*],
    table.cell(fill: green.lighten(60%))[Normal X],
    table.cell(fill: green.lighten(60%))[Normal Y],
    table.cell(fill: green.lighten(60%))[Normal Z],
    table.cell(fill: gray)[],
    table.cell(stroke: none)[`RGB8_SNORM`],
    table.cell(fill: yellow.lighten(60%))[*Material*],
    table.cell(fill: yellow.lighten(60%))[Reflectivity $[0, 1]$],
    table.cell(fill: yellow.lighten(60%))[Smoothness $[0, 1]$],
    table.cell(fill: yellow.lighten(60%))[Metallic $[0, 1]$],
    table.cell(fill: yellow.lighten(60%))[Emissive $[0, 32]$],
    table.cell(stroke: none)[`RGBA16_FLOAT`],
    table.cell(fill: orange.lighten(60%))[*Motion*],
    table.cell(fill: orange.lighten(60%))[Motion X],
    table.cell(fill: orange.lighten(60%))[Motion Y],
    table.cell(fill: orange.lighten(60%))[Water Mask],
    table.cell(fill: gray)[],
    table.cell(stroke: none)[`RGB16_FLOAT`],
    table.cell(fill: red.lighten(60%))[*Depth*],
    table.cell(fill: red.lighten(60%))[Linear Z],
    table.cell(fill: gray)[],
    table.cell(fill: gray)[],
    table.cell(fill: gray)[],
    table.cell(stroke: none)[`R16_UNORM`],
    table.cell(fill: purple.lighten(60%))[*Z-buffer*],
    table.cell(fill: purple.lighten(60%))[Hyperbolic Z],
    table.cell(fill: gray)[],
    table.cell(fill: gray)[],
    table.cell(fill: gray)[],
    table.cell(stroke: none)[`D32_UNORM`],
  ),
  caption: [Structure of the G-buffer of _Teardown_.],
) <gbuffer-teardown>

#figure(
  table(
    columns: (auto, auto),
    stroke: none,
    image("./img/teardown-gbuffer/01-color-pass-albedo.jpg"), image("./img/teardown-gbuffer/02-color-pass-normal.jpg"),
    [Base colour], [Normal],
    image("./img/teardown-gbuffer/03-color-pass-mat-rgb.jpg"),
    image("./img/teardown-gbuffer/03-color-pass-mat-alpha.jpg"),

    [Material (RGB)], [Material (A)],
    image("./img/teardown-gbuffer/04-color-pass-vel2.jpg"),
    image("./img/teardown-gbuffer/05-color-pass-linear-depth.jpg"),

    [Motion], [Linear Depth],
  ),
  kind: image,
  caption: [G-buffer of the frame in @fig-teardown of _Teardown_. @teardown-game
    @teardown-frame-teardown],
) <gbuffer-teardown-image>

To draw the voxel objects, the game draws the geometry of a box. Then, in the
fragment @shader, a DDA is performed through a 3D texture. To speed it up, the
renderer uses 2 #glspl("mipmap") to be able to skip empty space faster. The
rendering is done front-to-back to avoid overdraw by checking against the linear
depth buffer. The observer might have noticed that in @gbuffer-teardown there
are two depth #glspl("buffer") (called Depth and Z-buffer). This is because
OpenGL cannot know the depth of each pixel, since they all lie inside the box
used to render each object. To work around this problem, _Teardown_ writes the
depth of the pixels in the linear depth @buffer and occasionally copies it to
the Z-buffer.

Using @deferred-rendering is known to pose a problem for transparency. It is
often needed to first render opaque geometry using deferred rendering and then
the transparent geometry in a front-to-back order using @forward-rendering
@transparency-deferred-shading. To simulate 50% transparency, _Teardown_ displays
such materials in a chequerboard pattern and other percentages use a blue-noise
texture. This technique is called screen-door transparency
@screen-door-transparency.

After the voxels are drawn, there are two more passes. One for the various
wires, which are drawn using "normal" geometry, and then the smoke and fire
particles. Since these are drawn using 2D textures, they alternate their normals
for each pixel to simulate depth and also use screen-door transparency. Once the
draw calls are done, the renderer applies a blur on the normal @buffer to smooth
out the edges of the voxels.

Now that the G-buffer is ready, the lighting is computed. This is done in three
steps:

1. *Ambient lighting*: Computes ambient occlusion of light
2. *Sun lighting*: Computes the light coming from the sun
3. *Local lighting*: Computes the light coming from objects (lamps, fire, etc.)

All of this is computed using ray tracing. To do that, a 3D texture of the whole
level is rendered by a very optimized and multithreaded procedure on the CPU.
For the Marina level, it is stored in a $1752 times 100 times 1500$ texture that
is 262 MB in size. To keep the size of it relatively small, it is stored as a
1-bit map, where each 8-bit @texel stores 2#super("3") voxels. Two mipmap
levels are added to the texture to accelerate the ray tracing.

To render ambient lighting, two rays per-pixel are cast in a random hemisphere
around each point. This uses what the renderer calls "super sparse" tracing
mode. This means that instead of using the fast voxel traversal algorithm by
#cite(<fast-voxel-traversal-algorithm>, form: "prose") (DDA), it will sample voxels at
regular intervals. But _Teardown_ also has a "sparse" tracing mode, where it
will sample voxels at regular intervals. Where they differ is that the "super
sparse" mode will first start at the lowest mipmap level and then progressively go
up. This is illustrated in @super-sparse.
Whether these rays hit anything will determine the ambient lighting of the pixel.

#figure(
  grid(
    columns: 2,
    gutter: 28pt,
    image("./img/teardown-gbuffer/raytrace-2.png"), image("./img/teardown-gbuffer/raytrace-3.png"),
  ),
  caption: [Illustration of the "sparse" tracing mode (on the left) and of the "super
    sparse" tracing mode (on the right) of _Teardown_. You can see that voxels can
    be missed when using this method as highlighted by the red circle.
    @teardown-frame-teardown],
) <super-sparse>

Once this is done, the sun lighting is computed. For each pixel, a ray is shot
towards the sun using the "sparse" method. If it hits something, the pixel is in
shadow and if it hits nothing, it is illuminated. Local lights are then computed
using geometry that covers the area on the screen that will impact lighting.
Visibility of those is computed by tracing a ray from the pixel to a random
point on the light's surface to see if there is anything in between. When doing
these calculations, only the irradiance is computed, meaning the colour is not
taken into account. This is because the @buffer can then be denoised to hide
artefacts that appear from the @stochastic nature of this method.

Now that the lighting is rendered and denoised, it is applied to the base colour
@buffer. Then, volumetric lighting is rendered. To do that, a ray is marched
from the camera and at each step, another ray is cast to check if the current
light source is visible, if yes, its colour is added to the texture. Since this
effect is expensive to compute, it is done at quarter resolution. This texture
is also denoised.

The final step that will be explained here is reflections. If the pixel's
reflectivity is above zero, a ray is shot in the direction of the reflection.
The roughness value of the pixel will change the position of this ray to
simulate blurry reflections. The ray will check for collisions for 32 units, if
nothing is hit, it will use the skybox colour. Otherwise, it will either use the
colour of the voxel if it is on screen or have a black reflection. This buffer is
then denoised and applied to the final texture which will then receive some
post-processing effects.

This will not be explained in depth in this bachelor thesis, but we can
highlight that one of these is @taa and a side effect of this process is that it
can act as a final denoising step. Other post-processing effects include:

- 2D sprites with transparency
- @bloom
- @dof
- @motion-blur
- @auto-exposure
- @colour-correction
- @tone-mapping
- @gamma-correction

=== GVOX Engine <gvox-title>

#figure(
  image("img/gvox_screenshot.jpg", width: 70%),
  caption: [Screenshot of _GVOX Engine_.],
) <gvox-screenshot>

_GVOX Engine_ is an open-source project started in 2022 by Gabe Rundlett. The
progress of the project is shared on Rundlett's _YouTube_ channel as well as his _Discord_ server.
It is a fully custom voxel engine built on top of an abstraction layer over
Vulkan called _Daxa_, which Gabe Rundlett also works on. _Daxa_ is made for
GPU-driven rendering and has a fully bindless API. This means that _GVOX Engine_ is
GPU-driven and almost everything related to rendering is done in compute
shaders. _Daxa_ also provides other utilities, such as an ImGui integration,
FSR2 that can upscale the rendered image while also applying some anti-aliasing
and a task graph that allows to handle synchronization with the @gpu for the
user @daxa-video.

_GVOX Engine_ is an interesting project to analyse because it uses ray-tracing
for the voxel rendering with global illumination. It is also using compute
shaders for the rendering, which is closer to what the media project of this thesis
will do compared to _Teardown_. Furthermore, the world is fully editable with
good update time.

Although _GVOX Engine_ is open-source, its codebase is quite large and the fact
that it uses _Daxa_ instead of Vulkan directly makes it more complex to
understand. This analysis will try to be as complete as possible, however it
will mostly focus on the information that can be gathered from tools such as _RenderDoc_ and
occasionally looking at the code when needed. Also, as of may 2024, this project
has switched to using hardware ray-tracing, the analysis will be performed on a
previous version that was still using a compute shader.

The first rendering step is the background, which uses an atmosphere simulation
algorithm implemented by #cite(<atmosphere-rendering>, form: "prose"). This is
done in a separate command than the rest of the rendering. The voxel rendering
command task graph starts.

The voxel rendering compute shaders can be broken down into 6 sections:

+ World simulation and updating
+ Primary rays computation
+ Particles rasterisation
+ Secondary rays (indirect lighting) computation
+ FSR 2
+ Blur

The voxel data is stored in a palette-compressed single level brickmap. Although
the details may differ, the concept is probably similar as the technique
presented by #cite(<novel-extended-brickmaps>, form: "prose"). The idea of
palette-compression in the context of voxel data, is to try to encode the
presence of a voxel in as little data as possible, and store the colour
and material information elsewhere, in a palette. This allows to reuse these
colour data, that are often relatively big. A naive way of storing colour would be
using 3 floats, which is 12 bytes for examples. In a worst case scenario where
each voxel has a different colour, this way of storing data is more costly.
However, as the data gets bigger and colours start to repeat, it quickly becomes
worthwhile. As a palette is a dynamic structure, memory allocation should be done
carefully. Furthermore, the indirection caused by the palette could negatively
impact memory caches, so the data structure should be designed around it
@palette-compression.

As mentioned above, in the first part of the rendering, the world is simulated and
updated in a compute shaders. This includes moving objects such as grass or
water simulation. It also includes updating the world using brushes that allow
to destroy or place voxels. After that, primary rays are shot. From this, 4
buffers are filled. First, the depth buffer that is stored in `R32_FLOAT`
format. Then a velocity buffer, in `R16G16B16A16_FLOAT` format. However, it remains
dark most of the time due to the limited number of moving objects. Normals are
stored in `R10G10B10A2_UNORM` format. Finally, a buffer called G-buffer
internally that is stored in `R32G32B32A32_UINT` format is filled. They can all
be seen in @gvox-buffers.

From what can be seen in _RenderDoc_,
it is not exactly clear what is stored in the G-buffer and in what layout,
since only the red and blue channels have data
in them. But Rundlett shared on _Discord_ that he was inspired by _kajiya_ which
is an "Experimental real-time global illumination renderer made with Rust and
Vulkan" @kajiya. This is the layout explained in their documentation:

- Albedo (8:8:8, with one byte to spare)
- Normal (11:10:11)
- Roughness & metalness (2xf16; could be packed more)
- Emissive (shared-exponent rgb9e5)

As the green and alpha channels are empty (which makes sense since normals are
stored in a separated buffer), we can assume that the G-buffer stores the base
colour in the red channel and the roughness and metalness in the blue channel.

#figure(
  table(
    columns: (auto, auto),
    stroke: none,
    image("img/gvox_depth.jpg"),
    image("img/gvox_normals.jpg"),
    [Depth],
    [Normals],
    table.cell(colspan: 2, image("img/gvox_gbuffer.jpg", width: 50%)),
    table.cell(colspan: 2, [G-buffer]),
  ),
  kind: image,
  caption: [Buffers filled by the primary rays in _GVOX Engine_.],
) <gvox-buffers>

One thing to note about normals, is that they are not computed based on the
faces on each voxel, but they are the same on the whole voxel. This is an
artistic choice to give a smoother feel to the lighting of the scene.

Now that the primary rays are cast, particles are rasterised. This includes
things like grass, flowers, fire, water, etc. They are rasterised and not ray
traced to improve rendering performance. As rasterisation does not have all the
same effects that raytracing has, particles all have a second pass to render
shadows.

Secondary rays are then cast to compute global illumination. Global
illumination is computed using a technique called ReSTIR which uses resampling
techniques for high-quality results @restir-paper.

To improve performance, AMD FidelityFX™ Super Resolution 2 (FSR 2) is applied.
This is a technology developed by AMD to upscale images while applying
anti-aliasing @fsr2-post. Contrary to NVIDIA DLSS 2.0, which only works on
NVIDIA #glspl("gpu"), FSR 2 works on any @gpu. This means that for example, on a
4K monitor, the image could be first rendered at a 2K resolution and then
upscaled to 4K using FSR 2.

Finally, bloom, @tone-mapping, @gamma-correction and other post processing
effects are applied. Bloom makes it so that brightly lit objects on the scene have a
glow around them, giving a better feel on how things are lit.

== Interviews <chap-interviews>

To help guide the project and to have accurate informations on current
solutions, authors of notable voxel projects where interviewed.

=== Questionnaire and interviewees presentation

==== Questionnaire

A questionnaire of eight questions was prepared to understand various aspect of
voxel rendering. These questions where either asked on a call, or sent by email
to be answered. These questions are:

+ What lead you to start working with voxels?
+ For you, what is the hardest part when working with voxels?
+ From your personal experience, what do you think is the future of voxel
  rendering techniques?
+ Based on what you've already done, what would be your suggestions to improve
  current voxel rendering?
+ Have you used HWRT? If yes, what is your personal experience with it related to
  voxels?
+ If you had to restart your project from scratch, is there anything that you
  would change?
+ What is your personal experience with voxel animations?
+ Is there anything we did not mention on the subject of voxel rendering that you
  think would be worth mentioning?

This questionnaire was asked to four persons that each have notable voxel
experience.

==== Felix "Xima" Maier

Felix Maier is a graphics programmer from Germany and the author of the _VoxelChain_ engine
which is built using WebGPU and runs in the browser @voxelchain. The engine
features global illumination computed using @cellular-automata and ray tracing
for reflection and refraction. It also has entity rendering with animations and
ray-traced sound propagation. Before that, Maier worked on adding HWRT to the
WebGPU implementation of Chromium @dawn-ray-tracing.

==== Daniel "frozein" Elwell

Daniel Elwell is a graphics and game programmer from the USA who worked on a
voxel engine called _DoonEngine_ @doon-engine. He then used the engine to create
a sandbox game called _Terra Toy_ @terra-toy. This engine uses OpenGL and has
per-voxel reflections. After _Terra Toy_, Elwell started working on a new engine
that uses Vulkan and HWRT. He is currently working on it, he showed in a recent
video that he added the option to have multiple kinds of data structures in the
BLASes.

==== Ethan Gore

Ethan Gore is the author of the appropriately named _Ethan Gore's Voxel Project_.
This project uses OpenGL to render a ton of voxels using rasterisation. It also
allows to edit big part of the scene at once in very little time. A notable
feature is the ability to import _Minecraft_ maps where each #glspl("texel") in the
texture of the voxels that are in the map are imported as one voxel.
Meaning that the map has a resolution that's sixteen times bigger than _Minecraft_.
This scale is achieved though a powerful @lod system and the fact that rasterisation
is fast on GPUs.

==== Gabe Rundlett

Gabe Rundlett is the creator of the _GVox Engine_, which is an open-source voxel
engine that features ray-traced global illumination and an editable terrain.
Recently, he added HWRT to the engine. He also developed the `.gvox` format with
Douglas Dwyer. This format allows to store voxels with multiple kinds of internal
data-structure for faster loading. He was recently hired by _Tuxedo Labs_,
creators of _Teardown_.

=== Results analysis

A common cause of people starting their own voxel project are other voxel games
such as _Minecraft_ or _Cube World_. Furthermore, the dynamic nature of voxel
worlds is attractive.

The most cited answer about the difficulty of working with voxels is memory
consumption. This leads to the usage of data structures that can compress
memory, especially if they have sparse data, but this slows down
updating speeds. The respondents agree that the trade off between memory
consumption and update speed should be carefully considered. Some users expect
Global Illumination from voxel games, making project potentially more difficult
to make. Also, the big number of techniques available in the field can cause
analysis paralysis.

The hope is for future techniques to have worlds that are even more dynamic. The
interviewees concur that brickmaps offer a good balance of memory consumption and
update speed. One path that should be explored is using rasterisation to speed
up primary rays.

As for current techniques, there is no consensus on whether storage could be
improved or not. However it is possible that respondents are not speaking of the same
metric. Denoising and upscaling could be made in a way to profit from the
simple shapes of cubes.

The experts don't agree on if HWRT can be faster than pure compute. The "black
box" nature of the TLAS, that is not designed for voxel data, is not optimal and
one written by hand could be faster. When doing HWRT, passing only surface data
to the GPU can help. HWRT is designed for the divergent nature of ray tracing,
meaning that it can use all of the threads of the @gpu more efficiently. The
recent NVIDIA #glspl("gpu") that use the Lovelace architecture #footnote([The RTX 4000 series for example.]) can
profit from shader execution reordering that could improve performance even
more. DirectX 12 also recently got the possibility to use Work Graphs, which
allows to start work on the @gpu from the @gpu, which could potentially avoid
the problem of having idle threads like when using pure compute. However, a test
was made to traverse a @bvh with them, and it was slower than HWRT.

As an advice for starting a new project having the possibility of rapid
prototyping was often mentioned. One way of doing it that was shared is to have
a second branch that is simpler and allows to try new things. Also, big modules
should be separated, allowing to change them without breaking an other part of
the program.

The respondents agreed that voxel animation is a very hard problem, even more
than voxel storage or ray tracing. One strategy that was cited is to voxelize a
mesh model or a distance field, which is a grid or mathematical representation where
each point stores the shortest distance to a surface or object within a given space.

== Summary

In this section, two voxel projects where analysed in depth, _Teardown_ and _GVOX Engine_.
Then, four notable authors of voxel project where interviewed.

From this, we learned that the raster pipeline can be used for voxel ray tracing
as shown in _Teardown_ and that global illumination could be achieved with the
compute pipeline using brickmaps.

During the interviews, we learned that brickmaps are an interesting option to
store voxels and that there is no consensus on if HWRT can be faster for voxels
than compute. Fast iteration time should also be a priority.

= Test Protocol

== Definition of Used Metrics <used-metrics>

=== Memory Usage

As mentioned in the interviews, memory usage is the most difficult part when
handling voxels, meaning that measuring this value is important. Memory access
is considerably slower than computation, meaning that keeping data small can
improve the cache efficiency of the program and in turn give a smoother
application. The measurements will be done on scenes of varying size to see how
it scales.

=== Voxel Count

Although it is directly related to the memory consumption, the amount of voxels
that can be displayed by the engine is an important number. As the focus on the
media project was more on the path tracing side, the data structure to store voxels
is not very complex.
This means that the number will not be very high compared to what could be done using a better data structure.
For example, _Teardown_ can store two billion voxels in its world texture (although it lacks any colour information).
But it is important to measure it to compare the value once and if improvements have been done.

=== Frame Time

The goal of the media project is to be an interactive application. Meaning that
the path tracing code should be able to run in real time. This can be tested by
measuring the time that a frame takes to render. The number of frames per second
is not used as it can be a misleading metric when doing comparisons, although it
is the reciprocal of seconds per frame.

The minimum for interactive applications is often placed at $33. overline(3) "ms"$ (or $30 "fps"$),
although going a bit lower is possible, it will feel considerably worse. $16. overline(6) "ms"$ (or $60 "fps"$)
is considered a smooth target nowadays, although the advent of screens with high
refresh rates can lead to even lower numbers.

The amount of work the GPU has to do can vary depending on the scene and the
camera position, meaning that this measurements will be done with multiple
camera position.

== Measurement Tools <measurement-tools>

=== Custom Tools

As the media project is not made in an off the shelf engine, some of these tools
will have to be custom made. One of these will be a frame time dumper, that will
take the frame time value on 128 frames and write them to a file, that
will then be used to compute values (average, median or other).

=== NVIDIA Nsight Graphics

NVIDIA Nsight Graphics is a tool that attaches itself to an application and
allows to measure its performance. It has tools such as GPU Trace and Trace
Analysis that can help find hotspots and the percentage of frame time of
shaders. It can also give explanations on performance issues. This tool is only
available for NVIDIA #glspl("gpu"), meaning that another one must be used of AMD #glspl("gpu").

=== AMD Radeon GPU Profiler

AMG Radeon GPU Profiler is an equivalent to Nsight Graphics for AMD #glspl("gpu"),
with a stronger focus on profiling, so it lacks the debugging features. It will
be helpful to profile the application on the AMD hardware where test will be
run.

== Measurement Platforms

To measure these numbers, two computers will be used.
One has an NVIDIA graphics card and the other has an AMD one.
The first one is a laptop and the other one a desktop.

=== AMD Desktop

#figure(
  table(
    columns: (auto, auto),
    align: horizon,
    [*OS*], [Windows 11 Pro (10.0.22631)],
    [*CPU*], [AMD Ryzen 9 7900X3D, 4401 Mhz, 12 Cores],
    [*GPU*], [AMD Radeon RX 7900 XT],
    [*GPU Driver Version*], [31.0.24033.1003],
    [*RAM*], [32 GB],
  ),
  caption: [Hardware specifications of the desktop.],
) <tab-amd>

=== NVIDIA Laptop

This laptop is a Lenovo Legion Slim 7 Gen 8 (16" AMD).
During the tests, it will be set to its maximum power profile.

#figure(
  table(
    columns: (auto, auto),
    align: horizon,
    [*OS*], [Windows 11 Pro (10.0.22631)],
    [*CPU*], [AMD Ryzen 7 7840HS, 3801 Mhz, 8 Cores],
    [*GPU*], [NVIDIA GeForce RTX 4060 Laptop],
    [*GPU Driver Version*], [552.44],
    [*RAM*], [32 GB],
  ),
  caption: [Hardware specifications of the laptop.],
) <tab-nvidia>

== Project Setup <project-setup>

To make reliable measurements, the app has a benchmark mode where the camera is fixed and ImGui is disabled.
The measurements will be done on two camera placements, once with per-voxel lighting
and once with per-pixel lighting. This will be explained in more details in @media-project.
To test if the technique called "russian roulette"
#footnote(["Russian roulette is a technique that can improve the efficiency of
Monte Carlo estimates by skipping the evaluation of samples that would make a small
contribution to the final result" @pbr-book[para. 2.2.4]]) speeds up the rendering,
it will be done on one of the camera positions and compared on both computers using Student's t-test.

The first placement, called "Bench 1", faces a wall and the rays don't have to travel a
lot of empty space compared to the second one, called "Bench 2", which is in a corner (see @fig-benches).
Bench 2 can also see the reflective ground which leads to more bounces.
This could mean that Bench 2 might have a higher frame time.

#figure(
  grid(
    columns: (auto, auto),
    gutter: 10pt,
    image("img/bench1.jpg"), image("img/bench2.jpg"),
  ),
  caption: [The two camera placements for the benchmarks. On the left "Bench 1" and on the right "Bench 2".],
) <fig-benches>

The measurements done using per-pixel lighting might also have worse performance,
since neighbouring pixels have branches that are less coherent,
which is problematic for the parallel nature of a GPU.

== Summary

In this section we explained what measurements will be made and how. The
measurements will be frame time, memory consumption and voxel count. As for the
tools, some will be custom made and for the rest, NVIDIA Nsight Graphics and AMD
Radeon GPU Profiler will be used.
The program will be tested on two computer, a laptop and a desktop.
This with multiple parameters to test the renderer under different conditions.

= Media Project <media-project>

This chapter will present the environment, limitations and the production of the media project.

== Development Environment

The project is programmed on a Windows 11 machine.
It first started as a Rust 1.78.0 project, which was developed using RustRover 2024.1 EAP by JetBrains.
The project used these packages:

- anyhow
- ash
- ash-window
- log
- nalgebra
- pretty_env_logger
- thiserror
- winit
- raw-window-handle
- vk-mem
- imgui
- imgui-winit-support
- bytemuck
- nalgebra-glm

And also packages that these ones use.

Then, the project was moved to Jai beta 0.1.091.
First Focus was used to edit the code, which is an editor made in Jai that has built-in syntax highlighting for the language. However, it does not support "go to definition" and other convenience features,
so Visual Studio Code was used with the Jails plugin afterwards.

Git is used for version control, and the repository is hosted on GitLab.

== Limitations

The goal of the project was in part to implement it using Vulkan,
meaning that a significant amount of the allowed time was spent on that.
Vulkan is a very verbose @api that forces the programmer to write many lines to get started.
The official Vulkan sample to display a triangle is 1200 lines long @vulkan-samples.
This took time from the voxel rendering part of the project meaning that not all of the goals where achieved.
The most important missing features are voxel models loading from `.vox` files and storage compression using Brickmaps.

Another limitation is the visual results achieved by the path tracing.
Although it is path tracing, it is very noisy and the accumulation is restarted every time the camera moves.
These are points that should be fixed to be able to use the renderer in a final product.

== Production

=== Rust and Vulkan Tutorials

Before working on voxel rendering, a Vulkan project has to be setup as a base.
Since at the time Rust was the planned programming language,
a decision had to be made on the Vulkan wrapper that would be used.
There are three main options available:
(1) Vulkano, which is a relatively high-level and safe wrapper.
(2) Ash, a lower-level wrapper that still provide some convenience by, for example, returning ```rust Vec<T>``` instead of slices.
(3) Vulkanalia, also a lower-level wrapper that is heavily inspired by Ash.
The key difference is that Vulkanalia has a very clear distinctions between the Vulkan bindings in the vulkanalia-sys crate and the wrapper in the vulkanalia crate.
Since the goal is to be able to distinguish when Vulkan is used versus the bindings, this is a good thing.

So the choice was made to use Vulkanalia.
The plan was to follow a tutorial named "Vulkan Tutorial" @vulkan-tutorial
and conveniently, a version using Vulkanalia exists @vulkan-tutorial-rust.
Finishing this tutorial lead to the image in @fig-vulkan-tutorial.
The Rust project has seventy five dependencies and sub-dependencies.
The total number of lines of code is roughly two thousands.

#figure(
  image("img/vulkan-tutorial.jpg", width: 70%),
  kind: image,
  caption: [Screenshot of a frame rendered using the Vulkan renderer made with Vulkan Tutorial.],
) <fig-vulkan-tutorial>

Once this was done, another tutorial was followed called "VulkanGuide" @vulkan-guide.
This tutorial goes further than Vulkan Tutorial by,
for example, starting out with compute pipeline before a triangle one.
It also uses more libraries such as vk-bootstrap, Vulkan Memory Allocator (VMA) and ImGui.

The library vk-bootstrap does not have a Rust equivalent,
but most of the benefits it brings could be replaced by functions made in Vulkan Tutorial.
However, when it came to using VMA, there were two options.
Either vk-mem, which is a wrapper around VMA, or gpu-allocator, which is a custom allocator made directly in Rust.
The problem is that both of those do not use the types brough by Vulkanalia,
but the ones from Ash.
Meaning that it is not possible to use them with Vulkanalia.
At this point, the choice was made to transition the project from Vulkanalia to Ash to be able to use one of these allocators.

Now that the project used Ash, an attempt with the gpu-allocator library
was made since it seemed to be the standard for Rust projects.
But it was changed to vk-mem as it did not have separate functions to allocate images and buffers,
and the goal was to avoid issues from anything that could go wrong from this.

Then, ImGui needed to be added. Conveniently, a crate called imgui exists.
However, it does not wrap the backends that come with the C++ version.
They have their own backends, but they don't include a Vulkan one.
So to be able to render with Vulkan, the imgui-rs-vulkan-renderer crate was imported to the project.
The library requires to pass in the vk-mem allocator wrapped in a mutex in an atomically reference counted type.
To be able to free the allocator at the correct time, it was also wrapped in a ManuallyDrop: ```rust gpu_allocator: ManuallyDrop<Arc<Mutex<vk_mem::Allocator>>>```.

ImGui could now be rendered, but it still needed to respond to window events,
which was managed by the crate winit.
To do this, the crate imgui-winit-support exists in the imgui project,
however at the time, it was using winit 0.27 and the tutorial project used winit 0.29.
To circumvent this, the backend was forked and ported to use winit 0.29.

The passing of window handles in Rust is usually done via the raw-window-handle crate. It has a common type that windowing libraries can use to so that other libraries that
would want to use a window don't have to rely on a specific windowing library.
However, not every crate depend on the same version of raw-window-handle, like for example imgui-winit-support.
So I had to enable a feature in winit to ask it to use version 0.5 of raw-window-handle.

The VulkanGuide tutorial was not completed
as not every feature presented were necessary for voxel rendering.
ImGui and compute shaders were the most important ones.
The base of the triangle pipeline was still implemented.

The image in @fig-vulkan-guide is the one that the project had when the tutorial was stopped.
In the middle you have Blender's test model Suzanne that is loaded from a GLTF 3D model file.
Its texture is a generated chequerboard.
The window on the top left is an ImGui window.
The render scale allows to change the virtual resolution of the image, which is then upscaled on the window.
The effect index allows to chose which compute shader to use for the background.
Data 1 through 4 are push constants parameters that the compute shaders use.
The first background is a gradient between colours provided in Data 1 and 2.
The second background is a sky with stars, with a background colour controlled by Data 1.

This project has approximately 3'300 lines of code and 159 dependencies.

#figure(
  image("img/vulkan-guide.jpg", width: 70%),
  kind: image,
  caption: [Screenshot of a frame rendered using the Vulkan renderer made with VulkanGuide.],
) <fig-vulkan-guide>

To start the real voxel renderer project, the tutorial one was copied to a new folder.
First, the mesh pipeline and GLTF loading was removed.
Then, dependencies were updated one by one. winit changed its @api,
requiring to change the structure of the project significantly.
The crate imgui-winit-support was upgraded,
meaning that raw-window-handle 0.6 was able to be used, but it still required a fork.
The crate vk-mem was upgraded to 0.4 and Ash to 0.38,
but this means that imgui-rs-vulkan-renderer could not be used any more
since it requires vk-mem 0.3 and Ash 0.37.
Now the choice was either write a custom ImGui renderer or downgrade every dependencies making all the previous work useless.

=== Jai and Vulkan Guide

At the same time, access to the private beta of the Jai programming language was granted.
As the dependency situation was a bit complicated with Rust, Jai was tried and then chosen for the media project.
The Jai compiler comes with a how_to folder which is a tutorial with code examples.
It also comes with a modules folder which contains the standard library of the language alongside bindings for various libraries such as SDL, ImGui and Vulkan.
A module in Jai is the word used for libraries, to not mix things up with C++ libraries #footnote("This was probably decided before C++20 modules").

The objective was to get back to the same point as the Rust project,
by having a compute pipeline and ImGui working.
First, a window was opened using SDL which was easy enough.
Then using Vulkan with the provided module worked well, until Vulkan 1.3 was required.
This module only provides Vulkan 1.0, so a project local module was created to update it.
Jai comes with a Bindings_Generator module that is used to generate the Vulkan bindings.
A `generate.jai` file is present in the project that reads the .h files and generates `.jai` files.
So Vulkan 1.3 headers were provided to the generator and some tweaking was made with the function that translates name when handling numbers.
These updated bindings where uploaded to their repository #footnote(link("https://gitlab.com/Stowy/jai-vulkan")).

Then VMA was added to the project.
Jai does not have VMA bindings by default, but Kofu on the Jai Discord posted their bindings.
These where then updated, reorganized a bit and also put in their own repository #footnote(link("https://gitlab.com/Stowy/jai-vma")).

With all of these assembled, a demo project using SDL and a Vulkan compute pipeline could be made.
The modules presented earlier where included in the project using Git Submodules.
This demo project was put in a repository as an example #footnote(link("https://gitlab.com/Stowy/jai-vulkan-sdl")).

The next step was to add ImGui.
Jai comes with ImGui bindings, but it is not its latest version,
and is not the one from the docking branch, that adds more features.
So it was updated and put into a separate repository #footnote(link("https://gitlab.com/Stowy/jai-imgui")).
But this was not enough, as just like in Rust, the ImGui bindings don't have the backends.
The next step was then to translate the C++ backend into Jai manually,
which is almost two thousand lines of code for the SDL2 and Vulkan backends.
After this was done, since it caught up to the Rust project and
has the code you would have at the end of chapter two of VulkanGuide,
it was also put in a repository #footnote(link("https://github.com/St0wy/Jai-Vulkan-Guide")).

=== Build Setup in Jai

In C++, to define a complete program, you cannot only rely on .cpp files.
Some of it is defined through the way the program is compiled,
through flags passed to the compiler.
To make this easier, third party tools like scripts, Makefiles or CMake are used.
But this requires to learn a new scripting language that are often quite complicated.

In Rust, the compilation is helped by a tool shipped with the compiler: Cargo.
The compilation is defined in a Cargo.toml file that can also have a list of dependencies.
This makes it easier than in C++, as it is a standard way that every project uses.
That being said, it is still limited to what Cargo allows you to enter in this file.

To help with these problems, Jai uses the approach of defining the build in a Jai file,
similarly to what Zig does.
Then, building the project is only a matter of running the command `jai build.jai`.
This method has the advantage of being able to share code between the build code and the runtime code.

In the media project, the build was setup as so: first, arguments are read to see if we want to do a debug, optimized or release build.
Then the shaders are compiled by invoking glslc.
After that, a build directory is created for the executable and compilation flags are set.
Finally, the build is performed. The code looks like that:

#figure(
  ```jai
  // #run tells the compiler to execute the code at compile time
  #run Build();
  Build:: () {
    w:= compiler_create_workspace();
    target_options:= get_build_options(w);
    args:= target_options.compile_time_command_line;

    build_optimized:= false;
    build_release:= false;

    for arg: args {
      if arg == {
        case "optimized";
          build_optimized = true;
        case "release";
          build_release = true;
      }
    }

    BuildShaders(debug = !build_optimized && !build_release);

    target_options.output_executable_name = "voxels";
    set_build_options(target_options, w);

    if build_release then BuildRelease(w);
    else if build_optimized then BuildOptimized(w);
    else BuildDebug(w);

    add_build_file("src/main.jai", w);

    set_build_options_dc(.{do_output=false});
  }
  ```,
  caption: [Build procedure of the `build.jai` file.],
) <lst-build-jai>

The compilation of shaders is quite easy thanks to Jai's ```jai run_command()```.

#figure(
  ```jai
  HasGlslc:: () -> bool {
    command:= break_command_into_strings("glslc --version");
    result, out, error:=
      run_command(..command, capture_and_return_output = true);
    return result.exit_code == 0;
  }

  BuildShaders:: (debug: bool = true) -> bool {
    if !HasGlslc() then return false;

    command:= ifx debug {
      break_command_into_strings("glslc voxel_render.comp -o voxel_render.comp.spv --target-env=vulkan1.3 -g -O0 -Werror");
    } else {
      break_command_into_strings("glslc voxel_render.comp -o voxel_render.comp.spv --target-env=vulkan1.3 -O -Werror");
    };

    result, out, error:=
      run_command(..command, "src/shaders", capture_and_return_output = true);

    if result.exit_code != 0 then return false;

    return true;
  }
  ```,
  caption: [Procedures to compile shaders in `build.jai`.],
) <lst-build-shaders>

The setup of the build directory creates the folder if it does not exists, and then copies the dll of used libraries.

#figure(
  ```jai
  SetupBuildDir:: (dir_path: string) {
    make_directory_if_it_does_not_exist("run");
    make_directory_if_it_does_not_exist(dir_path);

    #if OS == .WINDOWS {
      sdl_dll_path:= tprint("%/SDL2.dll", dir_path);
      if !file_exists(sdl_dll_path) then
        copy_file("modules/SDL/windows/SDL2.dll", sdl_dll_path);

      vma_dll_path:= tprint("%/VMA.dll", dir_path);
      if !file_exists(vma_dll_path) then
        copy_file("modules/jai-vma/windows/VMA.dll", vma_dll_path);
    }
  }
  ```,
  caption: [Procedures to setup the build directory in `build.jai`.],
) <lst-build-dir>

=== Voxel Ray Tracing

To start rendering voxel, they had to be passed to the compute shader.
This was done by passing a `VkDeviceAddress` via push constants.
The material model was pretty simple,
voxels are each ```jai u32``` with one byte to indicate the material and three for the colours.

To test the rendering, a world was generated using perlin noise.
The code was translated to Jai from the voxpopuli template for the "Ray Tracing with Voxels in C++ Series" blog posts @voxpopuli.
A lot of techniques implemented in the rest of this chapter are based on this series.

Once the voxel were accessible in the compute shader, it was time to render them.
The first step was to shoot primary rays and to display the voxel's normals.
The traversal of the volume was done using DDA, in a branchless manner @branchless-dda.
The results of this can be seen in @fig-normal-dda.

#figure(
  image("img/normals.jpg", width: 70%),
  caption: "Voxels rendered using the DDA algorithm.",
) <fig-normal-dda>

The generation of the world was really slow in debug mode, since it does not use LLVM to speed up build time.
It took upwards of 40 seconds to generate.
To remedy that, the library osor_cpu_dispatch was imported to have an easy way to do parallel fors.
This cut the generation time by ten.

After that, ray traced shadows were added alongside Phong shading.
The idea is to shoot a ray from the pixel collision point towards a light source and check if anything hits.

#figure(
  image("img/ray-traced-shadows.jpg", width: 70%),
  caption: "Ray traced shadows.",
) <fig-ray-traced-shadows>

This method of doing ray traced shadows gives hard shadows that are not realistic.
A method to have smooth shadows is to shoot multiple rays into a volume randomly and accumulate the result.
As it can start to be costly, from this point on, the app renders at half resolution and then upscales it on the screen.
Furthermore, since the shadows are accumulated on the buffer, we can add anti-aliasing by offsetting the sample position inside the pixel at each frame.
This is done using a Halton sequence that gives better coverage at low sample count.
At first the sampling of soft shadows was done using white noise.
To have better result at lower sample count and to improve denoising capability, it was changed to blue noise.

#figure(
  grid(
    columns: 2,
    gutter: 10pt,
    image("img/smooth-shadows-noisy.jpg"), image("img/smooth-shadows.jpg"),
  ),
  caption: "Smooth shadows. On the left is the first frame before accumulation, on the right is during accumulation.",
) <fig-smooth-shadows>

World loading and saving was also added.
If a world file exists, it is loaded, otherwise, the world is procedurally generated and then dumped into a file so it can be read back.

#figure(
  image("img/viking.jpg", width: 70%),
  caption: "Example of map loading. Map created by Debusschere, P. and voxelized by Bikker, J.",
) <fig-viking>

Although adding soft shadows helps with the realism of the image,
it still does not have indirect lighting.
For this, path tracing must be used.
The algorithm for it is not too complex, as explained in @lst-path-tracer.
On @fig-smooth-shadows, we can see that the colour is not uniform on each pixels,
because the seed of the random number generator is different for each of them.
As an artistic choice, it was decided to seed the random number generator with the position of the voxel,
making each of them have a uniform colour.
This makes the first frame of accumulation less usable, but after a few frames, it looks similar to per-pixel random values.

This technique would also allow to accumulate the lighting values on a world space buffer instead of a screen space one.
Thus allowing to keep the accumulation after the camera moves.
It was not implemented in this project.

#figure(
  grid(
    columns: 2,
    gutter: 10pt,
    image("img/path-trace-zero.jpg"), image("img/path-trace.jpg"),
  ),
  caption: "Per-voxel path tracing. On the left is the first frame before accumulation, on the right is during accumulation.",
) <fig-path-trace>

In this version the sky is a uniform colour.
To add variety, a .hdr map was used instead.
This replaces the background colour and also allows to use it as a light source.
Tone mapping and gamma correction were also added.

On @fig-env-map some voxels appear brighter than others because there is a sun with high values on the map, but that is over a very small area.
This means that there is a very low chance of hitting the sun.
One way to prevent that would be to use importance sampling on the sky, with a bias towards high intensity values, but this was not implemented.

#figure(
  grid(
    columns: 2,
    gutter: 10pt,
    image("img/env-map-noisy.jpg"), image("img/env-map.jpg"),
  ),
  caption: [
    Path tracing with an environment map as a light source.
    On the left is the first frame before accumulation, on the right is during accumulation.
  ],
) <fig-env-map>

As mentioned earlier, the lighting and randomising is done per-voxel.
But, with a few lines changed, it is possible to have per-pixel lighting.
So an option was added to enable that.
The amount of downscaling is also configurable between one (so no downscaling) to four.
It works by dividing the image resolution by this number.
It is also possible to enable russian roulette, which will randomly stop the bounce of rays that would be too long and therefore cost too much.
Finally, reflections where reintroduced using a blog post by #cite(<shadertoy-path-tracing>, form: "prose").

#figure(
  table(
    columns: (auto, auto),
    stroke: none,
    image("img/final.jpg"),
    image("img/final-pixel.jpg"),
    table.cell(colspan: 2, image("img/final-russian.jpg", width: 50%)),
  ),
  kind: image,
  caption: [Final render of the project. Top left is per-voxel lighting. Top right is per-pixel lighting. Bottom has russian roulette enabled.],
) <fig-final>

Up until now, the voxels were stored in a ```jai u32```.
But this limits the amount of data that can be stored per-pixel.
A solution could be to use an array of structs to store the voxels,
but this would significantly increase memory consumption.
A solution to this is to have two buffers.
One for the voxels and one for the materials.
The voxels will then index into the material buffer.
This technique is called palette-compression.

The size of the material index dictates the maximum size of the material buffer.
For this project, a size of one byte, so 256 materials was chosen.
Shaders don't support integers that are of a different size than four bytes,
so the one byte indices have to be extracted.
Given a voxel position, the material index is extracted using the method presented in @lst-material-index.

#figure(
  ```glsl
  uint Sample(ivec3 pos)
  {
    int signed_index = pos.x + pos.y * GRID_SIZE + pos.z * GRID_SIZE_2;
    if (signed_index < 0 || signed_index >= GRID_SIZE_3) return 0;

    uint index = uint(signed_index);
    uint packed_index = index / 4;
    uint sub_index = index % 4;

    uint packed_material_index =
      push_constants.voxel_buffer.voxels[packed_index];
    uint material_index = (packed_material_index >> (sub_index * 8)) & 0xff;

    return material_index;
  }
  ```,
  caption: [Code that extracts the material index from the voxel buffer. Four material indices are in one `uint`.],
) <lst-material-index>

This material system broke the world saving and loading and would require more code to fix.
Therefore, the choice was made to drop that feature and have the scene be generated by code at the start of the program every time.

The voxel material is presented in @lst-material.

#figure(
  ```jai
  VoxelMaterial:: struct
  {
    base_colour: Vector3;
    roughness: float32;
    emissive: Vector3;
    ior: float32;
    normal: Vector3;
    padding: float32;
    specular_colour: Vector3;
    specular_percent: float32;
  }
  ```,
  caption: [Material of the voxels. The padding is necessary because of @gpu layout requirements. `ior` is the index of refraction.],
) <lst-material>

== Summary

In this chapter, the environment, limitations and production of the media project where presented.
Starting from the tutorials in Rust, followed by the translation in Jai
and ending with the voxel path tracing.

The repository containing the code and the windows executable from the media project can be found on GitLab #footnote(link("https://gitlab.com/Stowy/voxel-ray-tracer")).

= Quantitative Analysis

This section will present the numbers measured from the media project specifically frame time, memory usage and voxel count as described in @used-metrics using the tools listed in @measurement-tools.

== Memory Consumption

=== RAM

The first version of the voxel buffer used `uint` to store them.
With one byte serving as the material index and then three bytes for the colour.
The voxels could be either diffuse, or reflective.
If they where reflective, the colour would be the tint of the reflection.

In an array of size 256#super("3") with each voxel being four bytes, it would take 67'108'864 bytes or 64 MiB.

But the current version of the project has a material type that is 64 bytes.
In the same array, it would make it 1'073'741'824 bytes long or 1 GiB.
This is why palette-compression was used, which means storing the materials in an array that is then indexed by the voxels.
Each voxels are then a one byte index, that is used to fetch the material.
This means that the material buffer is limited to 256 materials.
In our case of having 256 materials and a 256#super("3") voxel buffer, the voxel buffer is 16'777'216 bytes or 16 MiB.
The material buffer is 16'384 bytes long so 16 KiB.
This totals to 16'400 KiB.

This is not the only allocation in the program however,
measuring the total memory consumption of the program yields 145,43 MiB on the laptop
and 145,23 MiB on the desktop.

=== VRAM

Although the voxel buffer is allocated in RAM,
it is then transferred to the GPU and therefore copied in the VRAM.
But this is not the only thing allocated, there are two `R16G16B16A16_SFLOAT` buffers
for the HDR rendering and one `R8G8B8A8_UNORM` for LDR rendering.
There are also two other textures for blue noise and the environment map.
Finally, the swapchain also takes memory.
The LDR and HDR buffers have the size of the highest resolution screen on the computer.
The laptop has a 3200 x 2000 screen and the desktop a 2560 x 1440 screen.

The VRAM consumption was measured at 545'884 KiB on the desktop with an
AMD graphics card and 310'384 KiB on the laptop with an NVIDIA graphics card.
This difference could be cause by two things:
First, each driver has its own implementation that can do different allocations for the same code.
Second, it is possible that the AMD driver decided it was okay to allocate more, since the graphics card has 20 GB of VRAM compared to the 8 GB of the NVIDIA one.

== Voxel Count

The voxels are stored in a fixed array of size 256#super("3").
This allows to store 16'777'216 voxels.
The fixed size could be increased, but would take up more RAM and VRAM make the traversal slower.

This static array also means that it is not be possible to have an infinite world,
a more complex data structure allowing to load and unload chunks would be necessary.

== Frame Time

Frame time is the most important metric of this project since the goal is to have an interactive application.
This section will start by presenting the frame times under different conditions on the two computers used for measurements.
Then, it will determine if russian roulette really has a performance impact.

#figure(
  table(
    columns: (auto, auto, auto, auto, auto),
    align: horizon,
    table.header(
      [],
      [*Bench 1 #linebreak() Per-voxel*],
      [*Bench 1 #linebreak() Per-pixel*],
      [*Bench 2 #linebreak() Per-voxel*],
      [*Bench 2 #linebreak() Per-pixel*],
    ),

    [*Average*], [13,82 ms], [28,49 ms], [15,57 ms], [26,37 ms],
    [*Std. Dev*], [0.40 ms], [1,98 ms], [0,08 ms], [0,76 ms],
    [*Variance*], [0.0002 ms], [0,0039 ms], [0,0000 ms], [0,0006 ms],
  ),
  caption: [Frame time measurements done on the AMD Desktop (see @tab-amd).],
)

#figure(
  table(
    columns: (auto, auto, auto, auto, auto),
    align: horizon,
    table.header(
      [],
      [*Bench 1 #linebreak() Per-voxel*],
      [*Bench 1 #linebreak() Per-pixel*],
      [*Bench 2 #linebreak() Per-voxel*],
      [*Bench 2 #linebreak() Per-pixel*],
    ),

    [*Average*], [18,88 ms], [38,18 ms], [23.33 ms], [37,78 ms],
    [*Std. Dev*], [0,29 ms], [0,74 ms], [0,58 ms], [0,69 ms],
    [*Variance*], [0,0001 ms], [0,0006 ms], [0,0003 ms], [0,0005 ms],
  ),
  caption: [Frame time measurements done on the NVIDIA Laptop (see @tab-nvidia).],
)

As guessed in @project-setup, the second benchmark is slower.
This is probably due to the higher number of bounces and the
higher number of needed iterations though empty space.
We can also observe that per-pixel lighting is slower than per-voxel.
This is probably due to the fact the neighbouring pixels follow the same path (and therefore the same branches in the code) less often.
The standard deviation increases when using per-pixel lighting, this could lead to a choppy feel during gameplay.



#figure(
  table(
    columns: (auto, auto, auto),
    align: horizon,
    table.header(
      [],
      [*With Russian Roulette #linebreak() AMD Desktop*],
      [*Without Russian Roulette #linebreak() AMD Desktop*],
    ),

    [*Average*], [11,85 ms], [13,03 ms],
    [*Std. Dev*], [0,29 ms], [0,11 ms],
    [*Variance*], [0,0001 ms], [0,0000 ms],
  ),
  caption: [
    Frame time of the renderer with and without
    russian roulette enabled on the AMD desktop.
    Tests done on the Bench 1 camera placement with per-voxel lighting.
  ],
) <tab-russ-amd>

#figure(
  table(
    columns: (auto, auto, auto),
    align: horizon,
    table.header(
      [],
      [*With Russian Roulette #linebreak() NVIDIA Laptop*],
      [*Without Russian Roulette #linebreak() NVIDIA Laptop*],
    ),

    [*Average*], [17,66 ms], [19,85 ms],
    [*Std. Dev*], [0,70 ms], [0,51 ms],
    [*Variance*], [0,0005 ms], [0,0003 ms],
  ),
  caption: [
    Frame time of the renderer with and without
    russian roulette enabled on the NVIDIA laptop.
    Tests done on the Bench 1 camera placement with per-voxel lighting.
  ],
) <tab-russ-nvd>

Now, we will measure if russian roulette helps to lower the frame time.
@tab-russ-amd and @tab-russ-nvd show that the average 
frame time is higher without russian roulette, but we will need to show it statistically.
To do this, we will use Student's t-test.
This requires to define a null hypothesis that is the following:
there is no change in frame time when using russian roulette.
We also need an alpha that was defined to be 0,05.
After that, the 95% confidence interval was computed to know by how much the russian roulette changed frame time.

#figure(
  table(
    columns: (auto, auto, auto),
    align: horizon,
    table.header(
      [],
      [*AMD Desktop*],
      [*NVIDIA Laptop*],
    ),

    [*Null hypothesis*], [false], [false],
    [*95% C.I. Lower Bound*], [1,13 ms], [2,04 ms],
    [*95% C.I. Upper Bound*], [1,23 ms], [2,34 ms],
  ),
  caption: [Result of the Student's t-test, with lower and upper bound of the confidence interval.],
)

On both the AMD desktop and the NVIDIA laptop, the t-test yielded that the null hypothesis is false,
meaning that there is a change in frame time when using russian roulette.
The confidence interval indicates that russian roulette reduces frame time
by 1,18 #sym.plus.minus 0,1 milliseconds on the AMD desktop and
by 2,19 #sym.plus.minus 0,3 milliseconds on the NVIDIA laptop.
We can observe that NVIDIA profits from russian roulette more than AMD.

= Conclusion

In this thesis, we were able to study what voxels are, different methods to store them and what the general theory for path tracing is.
The techniques studied where the Fast Voxel Traversal Algorithm by #cite(<fast-voxel-traversal-algorithm>, form: "prose"), Sparse Voxel Octrees, Sparse Voxel Directed Acyclic Graphs, brickmaps and then HWRT.
We saw how different games and engine render and store voxels, such as _Minecraft_, _Cube World_, _Ethan Gore's Voxel Project_ and _Octo Voxel_.
Besides, people who worked on notable voxel projects were interviewed.
These persons are Felix Maier, author of the _VoxelChain_ engine, 
Daniel Elwell, author of _Terra Toy_,
Ethan Gore, author of _Ethan Gore's Voxel Project_ and
Gabe Rundlett, author of _GVox Engine_.

We could then define a test protocol that would serve to establish what measurements would be done and how.
These measurements where memory consumption, voxel count and frame time.
After that, a media project was developed using Jai and Vulkan, which first started as a Rust project.
We were able to see differences between the two languages and some of the difficulties that come with using Vulkan as a graphics @api.
Finally, we measured the performance of the renderer on two platforms and tested if russian roulette could decrease frame time or not.

== Results

During @chap-interviews we interviewed experts and they agreed that brickmaps have a good trade off
between memory consumption and update speed.
We also learned that there is no consensus on if HWRT can speed up voxel rendering.

With roughly five thousands lines of code in Jai,
a voxel renderer using Vulkan was successfully developed.
Before that, difficulty with the library ecosystem of Rust was encountered.
Especially with the fact the some libraries rely on specific Vulkan bindings
or Vulkan memory allocator crates.
The fact that the @api of the crates in the project evolved quickly meant that updating them was challenging.

To store voxels, a palette-compressed array was used.
This project was then measured and we could observe that memory consumption was higher in the AMD desktop.
The numbers also demonstrated that per-pixel lighting has a significant impact on performance
and that the position of the camera does too.

While this work sheds light on the subject and contributes to the overall understanding,
it is important to consider limiting factors such as
a lack of professional experience in the field
and the significant amount of time required to study both Vulkan and Jai.

== Future Work

Although a media project was developed for this thesis,
it does not showcase more complex techniques due to time limitations.

Firstly, a better data structure could be used to store voxels, that could profit from sparse data, such as brickmaps or SVDAGs.

Then, path tracing could be improved by adding refraction and using techniques such as ReSTIR to reduce noise and denoising techniques. Additionally, TAA could be used to reduce aliasing and denoise at the same time.

#bibliography("references.yml")

#show outline: set heading(outlined: true)

#outline(title: [List of Tables], target: figure.where(kind: table))

#outline(title: [List of Listings], target: figure.where(kind: raw))

#outline(title: [List of Figures], target: figure.where(kind: image))

#set page(flipped: true)
#show: appendices

#include "interviews.typ"