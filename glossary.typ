#import "@preview/glossarium:0.5.0": glspl

#let entry-list = (
  (
    key: "cpu",
    short: "CPU",
    long: "Central Processing Unit",
    description: [Processor of the computer where most of the computation happens.],
  ),
  (
    key: "gpu",
    short: "GPU",
    long: "Graphics Processing Unit",
    description: [Also commonly called graphics card (although they are different). It is
      responsible for computing graphics related tasks.],
  ),
  (
    key: "lod",
    short: "LOD",
    artshort: "an",
    long: "Level Of Detail",
    description: [Technique used to improve performance in video games. The idea is that distant
      objects are replaced by lower details version since they will be less visible.],
  ),
  (
    key: "stack",
    short: "stack",
    description: [Data structure where you can only add or remove items from the top. It follows
      the Last In, First Out (LIFO) principle, like a stack of plates.],
  ),
  (
    key: "api",
    short: "API",
    long: "Application Programming Interface",
    description: [The interface by which two computer programs communicate.],
  ),
  (
    key: "mesh",
    short: "mesh",
    plural: "meshes",
    description: [A digital representation of a three-dimensional object composed of vertices that
      make up triangles.],
  ),
  (
    key: "streaming",
    short: "streaming",
    description: [The process of continuously transmitting data over a network or from a storage
      device to a receiving device, allowing playback or use of the data without the
      need for downloading the entire file beforehand.],
  ),
  (
    key: "bitmask",
    short: "bit mask",
    description: [A binary pattern used to selectively manipulate individual bits within a binary
      data word.],
  ),
  (
    key: "id-gloss",
    short: "ID",
    long: "identifier",
    description: [Unique label or value assigned to distinguish individual entities within a
      system or dataset.],
  ),
  (
    key: "shader",
    short: "shader",
    description: [A program that runs on the @gpu to manipulate the rendering of graphics
      primitives, controlling aspects like vertex transformations, lighting, and pixel
      colour generation.],
  ),
  (
    key: "igpu",
    short: "integrated GPU",
    description: [On laptops that require a powerful graphics card, such as gaming laptop, they
      often have a dedicated @gpu (often NVIDIA or AMD) and an integrated @gpu, also
      called discrete @gpu (often Intel or AMD) that is located on the @cpu. This is
      mainly for power efficiency.],
  ),
  (
      key: "cuda",
      short: "CUDA",
      // long: "Compute Unified Device Architecture",
      description: [A platform that allows to write C++ programs that run on NVIDIA GPUs.],
    ),
  (
    key: "forward-rendering",
    short: "forward rendering",
    description: [A real-time rendering technique where objects are individually rendered with
      lighting calculations applied during the process. It is suitable for scenes with
      fewer dynamic lights but can become computationally intensive with more lights
      or objects.],
  ),
  (
    key: "deferred-rendering",
    short: "deferred rendering",
    description: [A graphics technique where scene geometry is first rendered into intermediate
      #glspl("buffer"), then lighting calculations are performed separately, allowing
      for efficient handling of complex lighting effects in real-time graphics
      applications.],
  ),
  (
    key: "mipmap",
    short: "mipmap",
    description: [A series of pre-generated textures derived from an original texture, each at a
      progressively lower resolution. They enhance rendering efficiency and image
      quality by providing optimized textures for objects viewed at different
      distances or sizes.],
  ),
  (
    key: "os",
    short: "OS",
    long: "Operating System",
    description: [Integral software that governs computer hardware, enabling application
      execution, resource allocation, and user interaction through services like
      memory management, process scheduling, and file handling.],
  ),
  (
    key: "stochastic",
    short: "stochastic",
    description: [Refers to systems or processes characterized by inherent randomness, introducing
      uncertainty into calculations or simulations across fields like mathematics,
      statistics, and computer science, often employing probabilistic models to
      account for variable factors.],
  ),
  (
    key: "buffer",
    short: "buffer",
    description: [In computer graphics, a buffer is a temporary holding area in memory, typically
      on the GPU, used to store data (like voxel information) or intermediate
      calculations (like depth values) during rendering. This ensures smooth data flow
      and optimizes the process.],
  ),
  (
    key: "taa",
    short: "TAA",
    long: "Temporal Anti-Aliasing",
    description: [An anti-aliasing technique that improves image quality by combining information
      from current and previous frames to reduce aliasing (jagged edges). This method
      offers performance benefits compared to traditional anti-aliasing methods.],
  ),
  (
    key: "bloom",
    short: "bloom",
    description: [A post-processing effect that simulates the visual phenomenon of bright light
      bleeding into surrounding areas, creating a soft glow.],
  ),
  (
    key: "dof",
    short: "depth of field",
    description: [The portion of an image that appears sharp, ranging from the near focus point to
      the far focus point. Controlled by aperture, focal length, and distance to the
      subject.],
  ),
  (
    key: "motion-blur",
    short: "motion blur",
    description: [A visual effect simulating the blurring of objects in motion, mimicking
      real-world camera exposure time and enhancing the perception of speed.],
  ),
  (
    key: "auto-exposure",
    short: "automatic exposure",
    description: [A technique that simulates a camera's automatic exposure adjustments during
      rendering. It analyses the scene's lighting and adjusts rendered colours to
      achieve a balanced and natural-looking image within the display's dynamic range.],
  ),
  (
    key: "colour-correction",
    short: "colour correction",
    description: [The process of adjusting the colour balance, contrast, and saturation of a
      rendered image to achieve a desired visual appearance. This can involve fixing
      colour casts, enhancing realism, or creating a specific artistic style.],
  ),
  (
    key: "tone-mapping",
    short: "tone mapping",
    description: [Tone mapping adjusts the wide range of lighting values from the rendered scene
      to a narrower range suitable for display on monitors with a limited dynamic
      range. This preserves details in highlights and shadows while creating a
      natural-looking image.],
  ),
  (
    key: "gamma-correction",
    short: "gamma correction",
    description: [A technique that adjusts the brightness of colours in an image to compensate for
      the non-linear response of monitors. This ensures images appear visually
      consistent with how we perceive light, making dark areas appear darker and
      bright areas appear brighter.],
  ),
  (
    key: "llvm",
    short: "LLVM",
    description: [A collection of modular and reusable compiler and toolchain technologies for
      developing compilers and related tools. It optimizes code at various stages,
      supports multiple languages and platforms, and generates high-performance
      machine code.],
  ),
  (
    key: "cellular-automata",
    short: "cellular automata",
    description: [Mathematical models consisting of a grid of cells, each in one of a finite
      number of states, that evolve through discrete time steps according to a set of
      rules based on the states of neighbouring cells. Used to simulate complex systems
      and processes in fields like computer science, physics, and biology.],
  ),
  (
    key: "pbr",
    short: "PBR",
    long: "Physically Based Rendering",
    description: [A rendering technique that simulates the physical properties of light and
      materials for realistic lighting and shading effects, making objects appear more
      natural.],
  ),
  (
    key: "pdf",
    short: "PDF",
    long: "Probability Density Function",
    description: [A function that describes the likelihood of a continuous random variable taking
      on a specific value, where the area under the curve represents the probability
      of the variable falling within a given range.],
  ),
  (
    key: "bvh",
    short: "BVH",
    long: "Bounding Volume Hierarchy",
    description: [A tree structure used in computer graphics and computational geometry to
      accelerate spatial queries, such as ray tracing and collision detection.
      Each node in the hierarchy represents a bounding volume that encapsulates a set of
      objects or smaller bounding volumes,
      allowing for efficient testing and exclusion of
      large groups of objects in complex scenes.],
  ),
  (
    key: "brdf",
    short: "BRDF",
    long: "Bidirectional Reflectance Distribution Function",
    description: [A function that defines how light is reflected at an opaque surface,
      describing the relationship between the incoming light direction and
      the outgoing reflection direction.
      The BRDF is essential in realistic rendering, as it helps determine the colour and
      intensity of reflected light,
      contributing to the material's appearance under different lighting conditions.],
  ),
  (
    key: "texel",
    short: "texel",
    description: [
      The smallest unit of a texture map,
      representing a single point of texture data applied to a 3D model's surface
      in computer graphics. If a pixel is a texture element, a texel is a texture element.
    ],
  ),
)